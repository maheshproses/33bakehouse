import * as React from "react";

import { cn } from "@/lib/utils";

export interface TextareaProps
  extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {}

const Textarea = React.forwardRef<HTMLTextAreaElement, TextareaProps>(
  //@ts-ignore
  ({ className, floatLabel, error, label, isRequired, ...props }, ref) => {
    return (
      <>
        {floatLabel ? (
          <div className="relative mb-4">
            <textarea
              id="floating_filled"
              className="block rounded-lg px-2.5 pb-2.5 pt-5 w-full text-sm text-gray-900 bg-gray-30 dark:bg-gray-700 border-2 border-b-2- border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              ref={ref}
              {...props}
            />
            <label
              htmlFor="floating_filled"
              className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-4 z-0 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4"
            >
              {label}
              {isRequired ? <span className="text-red-700">*</span> : null}{" "}
            </label>
            {error ? (
              <div className="text-red-700 text-xs mt-1">{error}</div>
            ) : null}
          </div>
        ) : (
          <div className="mb-4">
            {label && (
              <label
                htmlFor="floating_filled"
                className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-4 z-10 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4"
              >
                {label}
                {isRequired ? (
                  <span className="text-red-700">*</span>
                ) : null}{" "}
              </label>
            )}

            <textarea
              className={cn(
                "flex min-h-[80px] w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50",
                className
              )}
              ref={ref}
              {...props}
            />
            {error ? (
              <div className="text-red-700 text-xs mt-1">{error}</div>
            ) : null}
          </div>
        )}
      </>
    );
  }
);
Textarea.displayName = "Textarea";

export { Textarea };
