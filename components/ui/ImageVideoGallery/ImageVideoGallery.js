import React, { useState, useEffect } from "react";
import ImageGallery from "react-image-gallery";
import ImageZoom from "../ImageZoom/ImageZoom";

const ImageVideoGallery = ({ sourceArr }) => {

  const [finalSource, setfinalSource] = useState([]);
  const { productImages, productVideo } = sourceArr;

  if (productVideo?.length) {
    productVideo.map((vdo) => {
      Object.assign(vdo, {
        renderItem: (vdo) => _renderVideo(vdo),
      });
    });
  }

  useEffect(() => {
    let final = [];
    if (productImages?.length) {
      let tempProductImages = productVideo ? productImages.concat(productVideo) : productImages;

      final = tempProductImages.map((item) => {
        if (!item?.check && !item?.Video) {

          item["thumbnail"] = `${item?.thumbnail}`;
          (item["original"] = `${item?.original}`), (item["check"] = true);
        } else if (item?.Video) {
          item["thumbnail"] = "/play-button.png";
          item["check"] = true;
        }
        return item;
      });
    }

    if (!final.length)
      final.push({
        thumbnail: "/no_image.png",
        original: "/no_image.png",
      });

    setfinalSource(final);

  }, [productImages, productVideo]);


  function _renderVideo(item) {

    return (
      <div>
        <div className="video-wrapper">

          {
            item ? (
              <video width="100%" height="200" controls autoPlay={true} loop muted>
                <source
                  src={item.original}
                  type="video/mp4"
                />
              </video>
            ) : null
          }

        </div>
      </div>
    );
  }

  //properties for image gallery
  const properties = {
    showThumbnails: "true",
    infinite: "true",
    thumbnailPosition: "bottom",
    showNav: true,
    items: finalSource,
    showPlayButton: false,
    showFullscreenButton: false,
    disableThumbnailScroll: false,

    renderItem: (item) => (
      <>
        <ImageZoom item={item} />
      </>
    ),
  };

  return (
    <section className="image_video_gallery">
      <ImageGallery {...properties} />
    </section>
  );
};

export default ImageVideoGallery;
