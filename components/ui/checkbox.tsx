import React from "react";

interface Props {
  checked?: boolean;
  value?: string | number;
  disabled?: boolean;
  preventDefault?: boolean;
  size?: any;
  color?: string;
  textColor?: any;
  id:any
}

type LabelProps = {
  label: string;
};

type NativeAttrs = Omit<React.InputHTMLAttributes<unknown>, keyof Props>;
type CheckboxProps = NativeAttrs  & LabelProps & Props;


const Checkbox = ({ label, ...rest }: CheckboxProps) => {
  return (
    <>
      <div>
        <div className="flex items-center mb-4">
          <input
            type="checkbox"
            {...rest}
            className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
          />
        {label ? (
          <label
            htmlFor={rest.id}
            className="ms-2 text-sm font-medium text-gray-900 dark:text-gray-300"
          >
            {label}
          </label>
        ) : null}
      </div>
        </div>
    </>
  );
};

export default Checkbox;