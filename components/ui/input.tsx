import * as React from "react";

import { cn } from "@/lib/utils";
import { Label } from "./label";

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (
    //@ts-ignore
    {className, type, label, isRequired, error, floatLabel, leftLabel, prefixIcon, suffixIcon, ...props },
    ref
  ) => {
    return (
      <>
        {floatLabel ? (
          <div className="relative mb-4">
            <input
              type={type}
              id="floating_filled"
              className="block rounded-lg px-2.5 pb-2.5 pt-5 w-full text-sm text-gray-900 bg-gray-30 dark:bg-gray-700 border-2 border-b-2- border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              ref={ref}
              {...props}
            />
            <label
              htmlFor="floating_filled"
              className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-4 z-0 origin-[0] left-2.5 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-4"
            >
              {label}
              {isRequired ? <span className="text-red-700">*</span> : null}{" "}
            </label>
            {error ? (
              <div className="text-red-700 text-xs mt-1">{error}</div>
            ) : null}
          </div>
        ) : leftLabel ? (
          <div className="grid md:grid-cols-3 items-center mb-4">
            <div className="">
            {label && (
              <Label>
                {label}{" "}
                {isRequired ? <span className="text-red-700">*</span> : null}{" "}
              </Label>
            )}
            </div>
            <div className="md:col-span-2">
            <input
              type={type}
              className={cn(
                "flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50",
                className
              )}
              ref={ref}
              {...props}
            />
            {error ? (
              <div className="text-red-700 text-xs mt-1">{error}</div>
            ) : null}
            </div>
          </div>
        ) : (
          <div className="mb-4">
            {label && (
              <Label>
                {label}{" "}
                {isRequired ? <span className="text-red-700">*</span> : null}{" "}
              </Label>
            )}
            <div className="relative">
            { prefixIcon ? (
            <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
              <span className="text-gray-500 sm:text-sm">{prefixIcon}</span>
            </div>
            ): null }

            <input
              type={type}
              className={cn(
                prefixIcon ? "flex h-10 w-full rounded-md border border-input bg-background px-3 pl-7 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50" : 
                suffixIcon ? "flex h-10 w-full rounded-md border border-input bg-background px-3 pr-7 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50" :
                "flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50",
                className
              )}
              ref={ref}
              {...props}
            />
            { suffixIcon ? (
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
              <span className="text-gray-500 sm:text-sm">{suffixIcon}</span>
            </div>
            ): null }
            </div>
            {error ? (
              <div className="text-red-700 text-xs mt-1">{error}</div>
            ) : null}
          </div>
        )}
      </>
    );
  }
);
Input.displayName = "Input";

export { Input };
