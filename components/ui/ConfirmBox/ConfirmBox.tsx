import Cross from "@/app/icons/Cross";
import React from "react";
import './ConfirmBoxStyle.css';
//@ts-ignore
import * as Popover from "@radix-ui/react-popover";

function ConfirmBox({
  children,
  trigger,
  side = "top",
  contentAlign = "center",
  triggerParent = "span",
}: {
  children: React.ReactNode;
  trigger: string | React.ReactNode;
  side?: "top" | "left" | "bottom" | "right";
  contentAlign?: "center" | "right" | "left";
  triggerParent?: "div" | "span";
}) {
  return (
    <Popover.Root defaultOpen={false}>
      <Popover.Trigger asChild>
        {triggerParent == "span" ? (
          <span>{trigger}</span>
        ) : (
          <div>{trigger}</div>
        )}
      </Popover.Trigger>
      <Popover.Portal>
        <Popover.Content
          className="PopoverContent"
          sideOffset={5}
          side={side}
          contentAlign={contentAlign}
        >
          <div style={{ display: "flex", flexDirection: "column", gap: 10 }}>
            {/* <p className="Text" style={{ marginBottom: 10 }}>
            Dimensions
          </p> */}
          </div>
          <Popover.Close className="PopoverClose" aria-label="Close">
            <Cross
              color="gray"
              style={{
                position: "absolute",
                right: "3px",
                top: "3px",
                fill: "#707070",
              }}
            />
          </Popover.Close>
          {children}
          <Popover.Arrow className="PopoverArrow" />
        </Popover.Content>
      </Popover.Portal>
    </Popover.Root>
  );
}

export default ConfirmBox;
