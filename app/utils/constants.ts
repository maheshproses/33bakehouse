// import environment from "App/environment";
import environment from "../environment";
import configFunc from "./config";
const config = configFunc(environment);

export const NODE_API_URL = config.ApiUrl;

export const TOKEN_PREFIX = `${NODE_API_URL}33BAKEHOUSE__`;

export const TOKEN_S = "@_ISCCM_JWT@@$$$$";

export const LS_KEYS = {
  userData: 'user_data',
  userCart: '33Bakehouse_user_cart'
}

export const MESSAGES = {
  INVALID_LOGIN: "Invalid credentials, Please try again.",
  ALREADY_EXIST: "Name already exist !",
  SERVER_ERROR: "Something went wrong !",
};

export const CRYPTOJS_CODE = "sada092121[*]#piup"

export const publicPathName = `${config.publicBasePath}`

export const routeBaseUrl = `${config.baseUrl}`

export const ADDRESS_TYPE = {
  BILLING: "Billing",
  SHIPPING: "Shipping"
}



