import axios, { AxiosResponse } from "axios";
import { NODE_API_URL, TOKEN_PREFIX } from "./constants";

const fetchClient = () => {
  const defaultOptions = {
    baseURL: NODE_API_URL,
    headers: {
      "Content-Type": "application/json",
    },
  };

  let instance = axios.create(defaultOptions);

  instance.interceptors.request.use(async (config: any) => {
    try {
      let token = null;
      if (typeof window !== undefined) {
        token = localStorage?.getItem(TOKEN_PREFIX);
      }
      config.headers.Authorization = token;
    } catch (error) {
      console.log(error);
    }
    return config;
  });

  instance.interceptors.response.use(
    (response: AxiosResponse) => response,
    (err: any) => {
      
      if (err?.response?.status === 401) {
        window.location.href = "Login"
        localStorage.removeItem(TOKEN_PREFIX)
      }
      return Promise.reject(err.response);
    }
  );

  return instance;
};

export default fetchClient();