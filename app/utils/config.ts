//@ts-nocheck
let configData = {
  development: {
    ApiUrl: "http://localhost:9002",
    // ApiUrl: "http://192.168.1.62:9002",
    adminURL: "http://localhost:4200",
    baseUrl: "",
    publicBasePath: ""
  },
  staging: {
    ApiUrl: "http://prosesindia.in:9012",
    adminURL: "http://prosesindia.in/33bakehouse_admin/#/login",
    baseUrl: "",
    publicBasePath: ""
  },
  production: {
    // customerUrl: "http://prosesindia.in//",
    ApiUrl: "http://prosesindia.in:9012/",
    adminURL: "http://prosesindia.in/33bakehouse_admin/#/login",
    baseUrl: "/33bakehouse_new",
    publicBasePath: "/33bakehouse_new"
  },
};

export default function (env: string): {
  ApiUrl: string;
  adminURL: string;
  baseUrl: string;
  publicBasePath: string;
} {
  return configData[env];
}
