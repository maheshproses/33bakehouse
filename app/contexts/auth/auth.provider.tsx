import React, { useReducer } from 'react';
import AuthContext from "./auth.context"
import { LS_KEYS, TOKEN_PREFIX } from '@/app/utils/constants';

const isBrowser = typeof window !== 'undefined';
const INITIAL_STATE = {
  authenticated: isBrowser && !!localStorage.getItem(TOKEN_PREFIX),
  user_data:  isBrowser && !!localStorage.getItem(LS_KEYS.userData)
};

function reducer(state: any, action: any) {
  switch (action.type) {
    case 'SIGN_IN':
      return {
        ...state,
        authenticated: true,
        user_data: action.payload
      };
    case 'SIGN_OUT':
      return {
        ...state,
        authenticated: false,
      };
    default:
      return state;
  }
}

export const AuthProvider = ({ children }: any) => {
  const [authState, authDispatch] = useReducer(reducer, INITIAL_STATE);
  return (
    <AuthContext.Provider value={{ authState, authDispatch }}>
      {children}
    </AuthContext.Provider>
  );
};