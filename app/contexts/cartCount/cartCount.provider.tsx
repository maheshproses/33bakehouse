import React, { useReducer } from "react";
import CartCountContext from "./cartCount.context";

const isBrowser = typeof window !== "undefined";
const userData = isBrowser ? localStorage.getItem("user_data") : null;
const INITIAL_STATE = {
  count: isBrowser && userData ? JSON.parse(userData).cartCount : 0,
};
console.log(INITIAL_STATE,"INITIAL_STATE");



function reducer(state: any, action: any) {
  console.log(state,"state");
  
  
  let customerInfo: any = {};
  if (localStorage.getItem("user_data")) {
    //@ts-ignore
    customerInfo = JSON.parse(localStorage.getItem("user_data"));
  }

  const { type, payload } = action;
  
  switch (type) {
    case "INITIAL_SET_ITEM":
      customerInfo.cartCount = payload;
      localStorage.setItem("user_data", JSON.stringify(customerInfo));
      return { count: payload };

    case "ADD_ITEM":      
      
      customerInfo.cartCount = state.count + 1;
      localStorage.setItem("user_data", JSON.stringify(customerInfo));
      return { count: state.count + 1 };

    case "REMOVE_ITEM":
      customerInfo.cartCount = state.count - 1;
      localStorage.setItem("user_data", JSON.stringify(customerInfo));
      return { count: state.count - 1 };

    case "EMPTY_ITEMS":
      customerInfo.cartCount = 0;
      return { count: 0 };

    default:
      return state;
  }
}

export const CartCountProvider = ({ children }: any) => {
  const [cartCountState, cartCountDispatch] = useReducer(
    reducer,
    INITIAL_STATE
  );

  
  return (
    <CartCountContext.Provider value={{ cartCountState, cartCountDispatch }}>
      {children}
    </CartCountContext.Provider>
  );
};
