import api from "@/app/utils/api";

export const getAllCategory = () => {
  return api.get("/customerApi/getAllActiveCategory");
};

export const getAllProduct = () => {
  return api.get("/customerApi/getAllActiveProduct");
};

export const getCategoryByID = (id: number) => {
  return api.get(`/customerApi/getCategoryByID/${id}`);
};

export const getProductByID = (id: number) => {
  return api.get(`/customerApi/getProductByID/${id}`);
};

//add new cart
export const addCart = (body: any) => {
  return api.post("/cart/user-addcart", body);
};

//add customer address
export const addUserAddress = (body: any) => {
  return api.post("/user/adduseraddress", body);
};

export const getUserAddressList = (id: number) => {
  return api.get(`user/getUserAddressList/${id}`);
};

export const getCartList = () => {
  return api.get(`/cart/user-getCartByUserId`);
};

export const updateCart = (id: number, body: any) => {
  return api.put(`/cart/user-updateCartQty/${id}`, body);
};

export const deleteCart = (id: number) => {
  return api.delete(`/cart/user-deleteCartItems/${id}`);
};

//add or place order
export const addPlaceOrder = (body: any) => {
  return api.post("/order/placeOrder-user", body);
};

export const getOrders = () => {
  return api.get("/order/getAllOrderList");
};

export const updateProfile = (id: number, body: any) => {
  return api.put(`/customerApi/updateProfile/${id}`, body);
};

export const getProfileData = () => {
  return api.get(`/customerApi/getProfileData`);
};

export const deleteUserAddress = (id: number) => {
  return api.delete(`/customerApi/deleteUserAddress/${id}`);
};

export const updateUserAddress = (id: any, body: any) => {
  return api.put(`/customerApi/updateUserAddress/${id}`, body);
};

//add or place order
export const addWithoutLoginPlaceOrder = (body: any) => {
  return api.post("/order/withoutLoginplaceOrder-user", body);
};

export const getOrderTrack = (email: any) => {
  return api.get(`/order/getOrderTrack/${email}`);
};

export const productOrderPDF = (id: number) => {
  return api.get(`/order/productOrderPDF/${id}`);
};

export const SingleOrderByOrderNo = (orderNo: number) => {
  return api.get(`/order/SingleOrderByOrderNo/${orderNo}`);
};
