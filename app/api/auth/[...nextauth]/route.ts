import NextAuth, {
  User,
  type NextAuthOptions,
  Account,
  Profile,
} from "next-auth";

import GithubProvider from "next-auth/providers/github";
import AppleProvider from "next-auth/providers/apple";
import GoogleProvider from "next-auth/providers/google";
import FacebookProvider from "next-auth/providers/facebook";
import CredentialsProvider from "next-auth/providers/credentials";
import { AdapterUser } from "next-auth/adapters";
import { NODE_API_URL } from "@/app/utils/constants";
import api from "@/app/utils/api";
import { SocialLoginData, registerData } from "../../login";

const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      // The name to display on the sign in form (e.g. 'Sign in with...')
      id: "credentials",
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      // You can pass any HTML attribute to the <input> tag through the object.
      credentials: {
        email: {
          label: "email",
          type: "email",
          placeholder: "jsmith@example.com",
        },
        password: { label: "Password", type: "password" },
        name: { label: "Name", type: "text" },
        mobile: { label: "mobile", type: "number" },
      },
      async authorize(credentials, req) {
        // You need to provide your own logic here that takes the credentials
        // submitted and returns either a object representing a user or value
        // that is false/null if the credentials are invalid.
        // e.g. return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
        // You can also use the `req` object to obtain additional parameters
        // (i.e., the request IP address)
        const res = await api.post(`/user/register`, credentials);


        // If no error and we have user data, return it
        // if (res.ok && user) {
        //   return user;
        // }
        // Return null if user data could not be retrieved
        return null;
      },
    }),
    AppleProvider({
      clientId: "33bakehouse-test",
      clientSecret:
        "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgDqgOf8Zy9NhuwykuAGgoiHsLevPY1sy0QfpJLiLUMmOgCgYIKoZIzj0DAQehRANCAAR4QcrB5Z30VZzXHAFvy6ERe21rm7G2LqS5JYIYJ9vWrR5zNFoePsXcGXX/6EOulfqTJNdPgqXTDxxXr9qqjUPY",
    }),
    GoogleProvider({
      clientId:
        "51107099588-i91tuirshf992q1lmdptqngjj336h50q.apps.googleusercontent.com",
      clientSecret: "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",
      // authorization:"https://appleid.apple.com/auth/authorize",
      // requestTokenUrl:'https://appleid.apple.com/auth/token'
    }),
    FacebookProvider({
      clientId: "652655160076591",
      clientSecret: "e9b19b6c742284d0cc454005ca434f5a",
    }),
  ],
  callbacks: {
    session: ({ session, token }) => {
      return {
        ...session,
        user: {
          ...session.user,
          id: token.id,
          randomKey: token.randomKey,
        },
      };
    },
    jwt: ({ token, user }) => {
      if (user) {
        const u = user as unknown as any;
        return {
          ...token,
          id: u.id,
          randomKey: u.randomKey,
        };
      }
      return token;
    },
    // @ts-ignore
    async signIn({
      account,
      profile,
    }: {
      account: Account | null;
      profile?: Profile | undefined;
    }) {
      if (account?.provider === "google") {

        let obj = {
          socialLogin: account?.provider,
          email: profile?.email,
          name: profile?.name,
        };
        let regData: any = await SocialLoginData(obj);
        // if (regData) {
        //   localStorage.setItem("user_data", regData);
        // }
        return profile;
      }
      if (account?.provider === "facebook") {
        let obj = {
          socialLogin: account?.provider,
          email: profile?.email,
          name: profile?.name,
        };
        let regData: any = await SocialLoginData(obj);
        // localStorage.setItem("user_data", regData);

        return profile;
      }
      if (account?.provider === "apple") {

        let obj = {
          socialLogin: account?.provider,
          email: profile?.email,
          name: profile?.name,
        };
        let regData: any = await SocialLoginData(obj);
        // localStorage.setItem("user_data", regData);

        return profile;
      }
      return true; // Do different verification for other providers that don't have `email_verified`
    },
  },
  secret: "thisissecret",
};

const handler = NextAuth(authOptions);
export { handler as GET, handler as POST };
