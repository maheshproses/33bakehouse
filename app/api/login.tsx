import api from "@/app/utils/api";

//admin next-auth login
export const registerData = (body: any) => {
  return api.post("/customerApi/user-registration", body);
};

export const SocialLoginData = (body: any) => {
  return api.post("/customerApi/user-socialLogin", body);
};

export const login = (body: any) => {
return api.post("/customerApi/user-login", body)
}

export const updatePassword = (body: any) => {
return api.post("/customerApi/ChangePassword", body)
}