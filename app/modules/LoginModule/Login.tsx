import { useContext, useState } from "react";
import router from "next/router";
import * as Yup from "yup";
import LoginForm from "./components/LoginForm";
import { useCart } from "@/app/shared/hooks/use-cart/use-cart.hook";
import { LS_KEYS, TOKEN_PREFIX } from "@/app/utils/constants";
import { toastAlert } from "@/app/shared/components/Layout";
import { login } from "@/app/api/login";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { handleServerError } from "@/app/utils/helpers";
import AuthContext from "@/app/contexts/auth/auth.context";
import CartCountContext from "@/app/contexts/cartCount/cartCount.context";
import { Formik } from "formik";
import { ToastContainer } from "react-toastify";

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .required("Email is required")
    .email("Please Enter Valid Email")
    .matches(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      "Please Enter Valid Email"
    ),
  password: Yup.string().required("Password is required"),
});

function Login() {
  const { authDispatch }: any = useContext(AuthContext);
  const { cartCountDispatch }: any = useContext(CartCountContext);

  const [passwordShown, setPasswordShown] = useState(false);
  const cart = useCart();

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  const backToSignUp = () => {
    router.push(`/Registration`);
  };

  const handleData = async (formValues: any) => {
    try {
      let cartArray = cart.items;

      let result = await login({ ...formValues, cartArray });
      cart.clear();

      if (result) {
        localStorage.setItem(TOKEN_PREFIX, result.data.data.token);
        localStorage.setItem(
          "user_data",
          JSON.stringify(result.data.data.user)
        );
        let userData = result.data.data;

        authDispatch({ type: "SIGN_IN", payload: userData });
        
        cartCountDispatch({
          type: "INITIAL_SET_ITEM",
          payload: result.data.data.user.cartCount,
        });
        router.push("/");

        toastAlert("success", result.data.data.msg);
      }
    } catch (error: any) {

      handleServerError(error);
    }
  };

  return (
    <>
      <div>
        <ToastContainer
          position="top-center"
          autoClose={1500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>

      <div className="items-center justify-center gap-6 rounded-lg p-8 md:grid lg:grid-cols-2 xl:grid-cols-3 py-20 ">
        <div className="col-start-2">
          <div className="rounded-lg border bg-card text-card-foreground shadow max-w-md m-auto">
            <div className="flex flex-col p-6 space-y-1">
              <h3 className="font-semibold tracking-tight text-4xl text-center py-16">
                Login
              </h3>

              <LoginForm
                loginSchema={loginSchema}
                passwordShown={passwordShown}
                togglePassword={togglePassword}
                handleData={handleData}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
