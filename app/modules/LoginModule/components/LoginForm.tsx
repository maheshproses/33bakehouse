import Apple from "@/app/icons/Apple";
import FaceBook from "@/app/icons/FaceBook";
import Google from "@/app/icons/Google";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { yupResolver } from "@hookform/resolvers/yup";
import { Formik } from "formik";
import { Eye, EyeOff } from "lucide-react";
import { signIn } from "next-auth/react";
import Link from "next/link";
import router from "next/router";
import { useForm } from "react-hook-form";

export const initValues: any = {
  email: "",
  password: "",
};

function LoginForm({
  loginSchema,
  passwordShown,
  togglePassword,
  handleData,
}: any) {
  const backToSignUp = () => {
    router.push("/Registration");
  };

  return (
    <>
      <Formik
        initialValues={initValues}
        validationSchema={loginSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleData(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          resetForm,
        }) => (
          <>
            <Input
              label="Email"
              type="email"
              isRequired
              placeholder="Enter email"
              onChange={handleChange}
              value={values.email}
              name="email"
              error={errors.email}
            />

            <div className="relative">
              <Input
                label="Password"
                type={passwordShown ? "text" : "password"}
                isRequired
                placeholder="Enter password"
                onChange={handleChange}
                value={values.password}
                name="password"
                error={errors.password}
              />
              <Button
                variant="outline"
                size="icon"
                className="absolute inset-y-6 right-0"
                onClick={togglePassword}
              >
                {passwordShown ? (
                  <Eye className="h-4 w-4" />
                ) : (
                  <EyeOff className="h-4 w-4" />
                )}
              </Button>
            </div>

            <div className="basis-1/2 flex flex-row items-center justify-between mb-6">
              <Button type="submit" variant="link">
                Forgot Password
              </Button>
              <Button type="submit" onClick={() => handleSubmit()}>
                Submit
              </Button>
            </div>
            <div className="d-flex justify-content-between mt-5">
              <Link href={`/`}>Back to Home</Link>
              <Button
                type="submit"
                variant="link"
                className="text-blue-400 text-center"
                onClick={backToSignUp}
              >
                Don&apos;t have an accout? - Create Account
              </Button>
            </div>

            <div className="relative flex flex-row justify-center my-6">
              <span className="bg-white px-2 relative z-10">or</span>
              <div className="h-px w-3/4 bg-gray-300 absolute z-0 inset-y-3"></div>
            </div>
            <div className="flex flex-row justify-center">
              <Button
                onClick={() =>
                  signIn("apple", {
                    callbackUrl: "https://b556-49-43-33-220.ngrok-free.app/",
                  })
                }
                className="w-1/3 text-white bg-gray-800 hover:bg-gray-900 rounded-lg p-3 bottom-0 mx-1"
              >
                <Apple size={20} color={"currentColor"} />
              </Button>
              <Button
                onClick={() => signIn("facebook", { callbackUrl: "/" })}
                className="w-1/3 text-white bg-blue-700 hover:bg-blue-800 rounded-lg p-3 bottom-0 mx-1"
              >
                <FaceBook size={20} color={"currentColor"} />
              </Button>
              <Button
                onClick={() => signIn("google", { callbackUrl: "/" })}
                // type="submit"
                className="w-1/3 text-white bg-red-700 hover:bg-red-800 rounded-lg p-3 bottom-0 mx-1"
              >
                <Google size={20} color={"currentColor"} />
              </Button>
            </div>
          </>
        )}
      </Formik>

      {/* <form>
        <Input
          label="Email"
          type="email"
          isRequired
          placeholder="Enter email"
          {...register("email")}
          error={errors.email?.message}
        />

        <div className="relative">
          <Input
            label="Password"
            type={passwordShown ? "text" : "password"}
            placeholder="Enter Password"
            isRequired
            {...register("password")}
            error={errors.password?.message}
          />
          <Button
            variant="outline"
            size="icon"
            className="absolute inset-y-6 right-0"
            onClick={togglePassword}
          >
            {passwordShown ? (
              <Eye className="h-4 w-4" />
            ) : (
              <EyeOff className="h-4 w-4" />
            )}
          </Button>
        </div>

        <div className="basis-1/2 flex flex-row items-center justify-between mb-4">
          <Button type="submit" variant="link">
            Forgot Password
          </Button>
          <Button type="submit" onClick={handleSubmit(onSubmitHandler)}>
            Submit
          </Button>
        </div>
        <div className="d-flex justify-content-between mt-5">
          <Link href={`/`}>Back to Home</Link>
          <Button
            type="submit"
            variant="link"
            className="text-blue-400 text-center"
            onClick={backToSignUp}
          >
            Don&apos;t have an accout? - Create Account
          </Button>
        </div>

        <div className="relative flex flex-row justify-center my-5">
          <span className="bg-white px-2 relative z-10">or</span>
          <div className="h-px w-3/4 bg-gray-300 absolute z-0 inset-y-3"></div>
        </div>
        <div className="flex flex-row justify-center">
          <Button
            onClick={() =>
              signIn("apple", {
                callbackUrl: "https://b556-49-43-33-220.ngrok-free.app/",
              })
            }
            className="w-1/3 text-white bg-gray-800 hover:bg-gray-900 rounded-lg p-3 bottom-0 mx-1"
          >
            <Apple size={20} color={"currentColor"} />
          </Button>
          <Button
            onClick={() => signIn("facebook", { callbackUrl: "/" })}
            className="w-1/3 text-white bg-blue-700 hover:bg-blue-800 rounded-lg p-3 bottom-0 mx-1"
          >
            <FaceBook size={20} color={"currentColor"} />
          </Button>
          <Button
            onClick={() => signIn("google", { callbackUrl: "/" })}
            // type="submit"
            className="w-1/3 text-white bg-red-700 hover:bg-red-800 rounded-lg p-3 bottom-0 mx-1"
          >
            <Google size={20} color={"currentColor"} />
          </Button>
        </div>
      </form> */}
    </>
  );
}

export default LoginForm;
