import type { NextPage } from "next";
import About from "./components/About/About";
import CategorySlider from "./components/CategorySlider/CategorySlider";
import Slider from "./components/Slider/Slider";
import TrendingSlider from "./components/TrendingSlider/TrendingSlider";
import { useEffect, useState } from "react";
import { getAllCategory, getAllProduct } from "@/app/api/apis";

const Home: NextPage = () => {
  const [poductList, setPoductList] = useState<any>([]);
  const [categoryList, setCategoryList] = useState<any>([]);

  useEffect(() => {
    getProductList();
    getCategoryList();

    return () => {};
  }, []);

  const getProductList = async () => {
    try {
      let productList = await getAllProduct();
      setPoductList(productList.data.data);
    } catch (error: any) {
    }
  };

  const getCategoryList = async () => {
    try {
      let categoryList = await getAllCategory();
      setCategoryList(categoryList.data.data);
    } catch (error: any) {
    }
  };

  return (
    <>
      {/* <Layout> */}

      <Slider />
      <TrendingSlider poductList={poductList} />
      <CategorySlider categoryList={categoryList} />
      <About />
      {/* </Layout> */}
    </>
  );
};

export default Home;
