import React from "react";
import {
  ArchiveBoxArrowDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "@heroicons/react/24/outline";

function Arrow(props: {
  disabled: boolean;
  left?: boolean;
  onClick: (e: any) => void;
}) {
  const disabeld = props.disabled ? " arrow--disabled" : "";

  return (
    <>
      <svg
        onClick={props.onClick}
        className={`arrow ${
          props.left ? "arrow--left" : "arrow--right"
        } ${disabeld}`}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
      >
        {props.left && (
          // <ChevronDoubleRightIcon className="h-6 w-6 text-blue-500" />
          // <AcademicCapIcon/>
          // <ArchiveBoxArrowDownIcon/>
          <ChevronLeftIcon className="text-gray-600" />
        )}
        {!props.left && (
          <ChevronRightIcon className="text-gray-600" />
          // <ChevronDoubleRightIcon className="h-6 w-6 text-blue-500" />
          //   <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z" />
        )}
      </svg>
    </>
  );
}

export default Arrow;
