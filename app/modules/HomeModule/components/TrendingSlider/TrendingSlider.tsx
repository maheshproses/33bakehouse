import React, { useEffect, useState } from "react";
import Link from "next/link";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/solid";
import { getAllProduct } from "@/app/api/apis";
// import { NODE_API_URL } from "@/utils/constants";
import { useKeenSlider } from "keen-slider/react";
import "keen-slider/keen-slider.min.css";
import Arrow from "../../Arrow";
import { NODE_API_URL } from "@/app/utils/constants";

const filteredItems = [
  {
    id: 1,
    img: "cake.jpg",
    description: "Cake",
    price: 200,
    link: "ProductDetail",
  },
  {
    id: 2,
    img: "cookies.jpg",
    description: "Cookies",
    price: 100,
    link: "ProductDetail",
  },
  {
    id: 3,
    img: "cupcake.jpg",
    description: "Cupcake",
    price: 500,
    link: "ProductDetail",
  },
  {
    id: 4,
    img: "cupcakes.jpg",
    description: "Cupcakes",
    price: 40,
    link: "ProductDetail",
  },
  {
    id: 5,
    img: "dessert.jpg",
    description: "Dessert",
    price: 140,
    link: "ProductDetail",
  },
  {
    id: 6,
    img: "gingerbread.jpg",
    description: "Gingerbread",
    price: 140,
    link: "ProductDetail",
  },
];

function TrendingSlider({ poductList }: any) {

  const [currentSlide, setCurrentSlide] = useState(0);
  const [loaded, setLoaded] = useState(false);

  const [productSliderRef, instanceRef] = useKeenSlider(
    {
      breakpoints: {
        "(min-width: 200px)": {
          slides: { perView: 1, spacing: 5 },
        },
        "(min-width: 768px)": {
          slides: { perView: 3, spacing: 15 },
        },
        "(min-width: 1190px)": {
          slides: { perView: 5, spacing: 15 },
        },
      },
      // slides: {
      //     perView: 5,
      //     spacing: 15,
      //     },
      loop: true,
      initial: 0,
      slideChanged(slider) {
        setCurrentSlide(slider.track.details.rel);
      },
      created() {
        setLoaded(true);
      },
    },
    [
      (slider) => {
        let mouseOver: boolean;
        slider.on("created", () => {
          slider.container.addEventListener("mouseover", () => {
            // console.log("i ma in");
            mouseOver = true;
          });
          slider.container.addEventListener("mouseout", () => {
            // console.log("i ma out");
            mouseOver = false;
          });
        });
      },
    ]
  );

  if (!poductList.length) {
    return <></>;
  }

  // const slideLeft = () => {
  //   let slider = document.getElementById("slider")!;
  //   slider.scrollLeft = slider.scrollLeft - 235;
  // };

  // const slideRight = () => {
  //   let slider = document.getElementById("slider")!;
  //   slider.scrollLeft = slider.scrollLeft + 235;
  // };

  return (
    <>
      <div className="trending h-full py-14 bg-zinc-200">
        <div className="container">
          <div className="title-btns relative">
            <h3>Product Slider</h3>
            <div className="btns relative">
              {/* {loaded && instanceRef.current && (
                <>
                  <Arrow
                    left
                    onClick={(e: any) =>
                      e.stopPropagation() || instanceRef.current?.prev()
                    }
                    disabled={currentSlide === 0}
                  />

                  <Arrow
                    onClick={(e: any) =>
                      e.stopPropagation() || instanceRef.current?.next()
                    }
                    disabled={
                      currentSlide ===
                      instanceRef.current?.track?.details?.slides?.length - 1
                    }
                  />
                </>
              )} */}

              {loaded && instanceRef.current && poductList && (
                <>
                  <Arrow
                    left
                    onClick={(e) =>
                      e.stopPropagation() || instanceRef.current?.prev()
                    }
                    disabled={currentSlide === 0}
                  />

                  <Arrow
                    onClick={(e) =>
                      e.stopPropagation() || instanceRef.current?.next()
                    }
                    disabled={
                      currentSlide ===
                      instanceRef.current.track.details.slides.length - 1
                    }
                  />
                </>
              )}
              {/* <button title="scroll left" onClick={slideLeft}>
                <ChevronLeftIcon />
              </button> */}
              {/* <button title="scroll right" onClick={slideRight}>
                <ChevronRightIcon />
              </button> */}
            </div>
          </div>
          <div className="navigation-wrapper">
            <div
              ref={productSliderRef}
              className="keen-slider"
              id="slider"
            >
              {poductList &&
                poductList.map((item: any, index: any) => (
                  <div
                    key={item.id}
                    className={`row-item keen-slider__slide number-slide${
                      index + 1
                    }`}
                  >
                    <Link href={`/ProductDetail/?id=${item.id}`} className="link">
                      <div className="item-header">
                        {/* <img src={item.img} alt="product" /> */}
                        <img
                          className="img-fluid"
                          src={
                            item.itemImages[0]?.image
                              ? `${NODE_API_URL}/static/itemImageFile/${item.itemImages[0]?.image}`
                              : "no_image.png"
                          }
                          alt="product"
                        />
                      </div>
                      <div className="item-description">
                        <p className="itemName">{item.itemName}</p>
                        {/* <p>{item.description}</p> */}
                        <p className="item-price">AED {item.basicPrice}</p>
                      </div>
                    </Link>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default TrendingSlider;
