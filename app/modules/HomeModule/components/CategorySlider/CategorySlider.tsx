import { NODE_API_URL } from "@/app/utils/constants";
import "keen-slider/keen-slider.min.css";
import { useKeenSlider } from "keen-slider/react";
import Link from "next/link";
import { useState } from "react";
import Arrow from "../../Arrow";

// const filteredItems = [
//   {
//     id: 1,
//     img: "cake.jpg",
//     description: "Cake",
//     price: 200,
//     link: "Products",
//   },
//   {
//     id: 2,
//     img: "cookies.jpg",
//     description: "Cookies",
//     price: 100,
//     link: "Products",
//   },
//   {
//     id: 3,
//     img: "cupcake.jpg",
//     description: "Cupcake",
//     price: 500,
//     link: "Products",
//   },
//   {
//     id: 4,
//     img: "cupcakes.jpg",
//     description: "Cupcakes",
//     price: 40,
//     link: "Products",
//   },
//   {
//     id: 5,
//     img: "dessert.jpg",
//     description: "Dessert",
//     price: 140,
//     link: "Products",
//   },
//   {
//     id: 6,
//     img: "gingerbread.jpg",
//     description: "Gingerbread",
//     price: 140,
//     link: "Products",
//   },
// ];

function CategorySlider({ categoryList }: any) {

  const [currentSlide, setCurrentSlide] = useState(0);
  const [loaded, setLoaded] = useState(false);

  const [categorySliderRef, instanceRef] = useKeenSlider(
    {
      breakpoints: {
        "(min-width: 200px)": {
          slides: { perView: 1, spacing: 5 },
        },
        "(min-width: 768px)": {
          slides: { perView: 3, spacing: 15 },
        },
        "(min-width: 1190px)": {
          slides: { perView: 5, spacing: 15 },
        },
      },
      // slides: {
      //   perView: 2,
      //   spacing: 15,
      // },
      loop: true,
      initial: 0,

      slideChanged(slider) {
        setCurrentSlide(slider.track.details.rel);
      },
      created() {
        setLoaded(true);
      },
    },
    [
      (slider) => {
        let mouseOver: boolean;
        slider.on("created", () => {
          slider.container.addEventListener("mouseover", () => {
            mouseOver = true;
          });
          slider.container.addEventListener("mouseout", () => {
            mouseOver = false;
          });
        });
      },
    ]
  );

  if (!categoryList.length) {
    return <></>;
  }

  // const slideLeft = () => {
  //   let categoryslider = document.getElementById("categoryslider")!;
  //   categoryslider.scrollLeft = categoryslider.scrollLeft - 235;
  // };

  // const slideRight = () => {
  //   let categoryslider = document.getElementById("categoryslider")!;
  //   categoryslider.scrollLeft = categoryslider.scrollLeft + 235;
  // };

  return (
    <>
      <div className="trending h-full py-14">
        <div className="container">
          <div className="title-btns">
            <h3 className="text-[1.5rem] font-light text-blue-500">
              Category Slider
            </h3>
            <div className="btns relative">
              {loaded && instanceRef.current && categoryList && (
                <>
                  <Arrow
                    left
                    onClick={(e) =>
                      e.stopPropagation() || instanceRef.current?.prev()
                    }
                    disabled={currentSlide === 0}
                  />

                  <Arrow
                    onClick={(e) =>
                      e.stopPropagation() || instanceRef.current?.next()
                    }
                    disabled={
                      currentSlide ===
                      instanceRef.current.track.details.slides.length - 1
                    }
                  />
                </>
              )}
              {/* <button title="scroll left" onClick={slideLeft}>
                <ChevronLeftIcon />
              </button>
              <button title="scroll right" onClick={slideRight}>
                <ChevronRightIcon />
              </button> */}
            </div>
          </div>
          <div className="navigation-wrapper">
            <div ref={categorySliderRef} className="keen-slider" id="slider">
              {categoryList.map((item: any, index: any) => (
                <div
                  key={item.id}
                  className={`row-item keen-slider__slide number-slide${
                    index + 1
                  }`}
                >
                  <Link
                    href={`/Products/?categoryId=${item.id}`}
                    className="link"
                  >
                    <div className="item-header">
                      {/* <img src={item.image} alt="product" /> */}
                      <img
                        className="img-fluid"
                        src={
                          item.image
                            ? `${NODE_API_URL}/static/categoryImage/${item.image}`
                            : "no_image.png"
                        }
                        alt="product"
                      />
                    </div>
                    <div className="item-description">
                      <p className="itemName">{item.title}</p>
                      {/* <p>{item.description}</p> */}
                      {/* <p className="item-price">AED {item.price}</p> */}
                    </div>
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default CategorySlider;
