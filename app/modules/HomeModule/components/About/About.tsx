import { publicPathName } from "@/app/utils/constants";
import React from "react";

const About = () => {
  return (
    <div className="container py-5">
      <div className="flex items-center xl:flex-row flex-col">
        <div className="basis-1/2 gap-4 xl:basis-1/2">
          <div className="xl:p-10 text-gray-500 p-0">
            <div className="AboutSection">
              <span className="text-2xl text-bhsecondary font-bold">
                Welcome FoodBuzz
              </span>
              <h3 className="">About Restaurant History!</h3>
            </div>
            <div className="section-wrapper">
              <p>
                Synergistca conceptualize leveraged inte ectual capital through
                virtual and the a and Proactiviely initiate lrisk highyield a
                platforms after realtme froms growth that awesome tently matrix
                robusti method powerm foistcky Sytical concepta leved ntelectua
                anding capita hrough virtua nteectua that capta the and
                Proactiviely initiate low risk highyield platforms after realtme
                froms that there andin Proctive initate low risk high yeld
                platform aftering Conven iterate optimal paradigms reliable
                techno
              </p>
            </div>
          </div>
        </div>
        <div className="basis-1/2 gap-4 xl:basis-1/2 py-5">
          <div>
            <img src={`${publicPathName}/macaroons.jpg`} alt="about-food" />
          </div>
        </div>
       
      </div>
    </div>
  );
};

export default About;
