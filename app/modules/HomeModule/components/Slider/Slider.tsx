import { Button } from "@/components/ui/button";
import "keen-slider/keen-slider.min.css";
import { useKeenSlider } from "keen-slider/react";
import React, { useState } from "react";
import Arrow from "../../Arrow";
import { publicPathName } from "@/app/utils/constants";

const sliderData = [
  {
    id: 1,
    img: "cupcake.jpg",
    description: "Enjoy delicious food with family",
    title: "Enjoy Food Enjoy Life",
    subtitle: "Praline with Nut",
    productName: "Break O’Clock",
  },
  {
    id: 2,
    img: "cake.jpg",
    description: "EXCLUSIVE CHOCOLATE & SWEETIES",
    title: "Stay Healthy Stay Strong",
    subtitle: "Passion Fruit Candy",
    productName: "Carl Marletti",
  },
];

function Slider() {
  const [currentSlide, setCurrentSlide] = React.useState(0);
  const [loaded, setLoaded] = useState(false);

  const [sliderRef, instanceRef] = useKeenSlider(
    {
      loop: true,
      initial: 0,
      slideChanged(slider) {
        setCurrentSlide(slider.track.details.rel);
      },
      created() {
        setLoaded(true);
      },
    },
    [
      (slider) => {
        let timeout: ReturnType<typeof setTimeout>;
        let mouseOver: boolean;
        function clearNextTimeout() {
          clearTimeout(timeout);
        }
        function nextTimeout() {
          clearTimeout(timeout);
          if (mouseOver) return;
          timeout = setTimeout(() => {
            slider.next();
          }, 10000);
        }
        slider.on("created", () => {
          slider.container.addEventListener("mouseover", () => {
            // console.log("i ma in");
            mouseOver = true;
            clearNextTimeout();
          });
          slider.container.addEventListener("mouseout", () => {
            // console.log("i ma out");
            mouseOver = false;
            nextTimeout();
          });
          nextTimeout();
        });
        slider.on("dragStarted", clearNextTimeout);
        slider.on("animationEnded", nextTimeout);
        slider.on("updated", nextTimeout);
      },
    ]
  );

  // const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
  //   initial: 0,
  //   slideChanged(slider) {
  //     setCurrentSlide(slider.track.details.rel);
  //   },
  //   created() {
  //     setLoaded(true);
  //   },
  // });

  return (
    <>
      <>
        <div className="navigation-wrapper">
          <div ref={sliderRef} className="keen-slider">
            {sliderData.map((item: any, index: number) => (
              <div
                key={item.id}
                className={`keen-slider__slide text-center justify-content-center align-items-center${index + 1}`}
              >
                <div className="slider">
                  <div>
                    <div className="w-full xl:h-[500px] md:h-[240px] relative">
                      <div className="absolute m-auto left-0 right-0 top-0 bottom-0 ">
                        <div className="justify-center flex-col items-center xl:mt-24 sm:mt-5 ">
                          <strong className="title py-0 pt-10 lg:py-3 sm:py-0 ">
                            {item.title}
                          </strong>
                          <h1 className="uppercase lg:text-[65px] py-0 font-bold text-bhsecondary text-[35px] leading-10 md:leading-normal md:text-[45px] xl:text-[65px]">
                            {item.subtitle}
                          </h1>
                          <h2 className="md:text-[35px] md:m-0 sm:p-0 py-0 lg:py-5">
                            {item.productName}
                          </h2>
                          <div className="txt md:m-0 md:p-0 lg:py-5">
                            {item.description}
                          </div>
                          <div className="flex justify-center items-center">
                            <Button
                              variant="outline"
                              className="btnSlider shop   hover:text-white hover:bg-bhsecondary"
                            >
                              Shop Now
                            </Button>
                          </div>
                        </div>
                      </div>
                      <div>
                        <img src={`${publicPathName}/${item.img}`} alt="product" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          {loaded && instanceRef.current && (
            <>
              <Arrow
                left
                onClick={(e: any) =>
                  e.stopPropagation() || instanceRef.current?.prev()
                }
                disabled={currentSlide === 0}
              />

              <Arrow
                onClick={(e: any) =>
                  e.stopPropagation() || instanceRef.current?.next()
                }
                disabled={
                  currentSlide ===
                  instanceRef.current.track.details.slides.length - 1
                }
              />
            </>
          )}
          {loaded && instanceRef.current && (
            <div className="absolute right-0 left-0 flex items-center bottom-[10px] justify-center">
              {[
                //@ts-ignore
                ...Array(
                  instanceRef.current.track.details.slides.length
                ).keys(),
              ].map((idx) => {
                return (
                  <button
                    key={idx}
                    onClick={() => {
                      instanceRef.current?.moveToIdx(idx);
                    }}
                    className={"dot" + (currentSlide === idx ? " active" : "")}
                  ></button>
                );
              })}
            </div>
          )}
        </div>
      </>
    </>
  );
}

export default Slider;
