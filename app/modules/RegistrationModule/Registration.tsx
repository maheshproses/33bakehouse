import React, { useState } from "react";
import RegistrationForm from "./components/RegistrationForm";
import * as Yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { handleServerError } from "@/app/utils/helpers";
import { ToastContainer } from "react-toastify";
import { registerData } from "@/app/api/login";
import { toastAlert } from "@/app/shared/components/Layout";
import { useRouter } from "next/router";
import { useCart } from "@/app/shared/hooks/use-cart/use-cart.hook";

const schema = Yup.object().shape({
  name: Yup.string().max(255).required("Name is required"),
  mobile: Yup.string()
    .matches(
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      "Phone number is not valid"
    )
    .typeError("Please Enter Valid Phone number"),
  email: Yup.string()
    .required("Email is required")
    .email("Please Enter Valid Email")
    .matches(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      "Please Enter Valid Email"
    ),
  password: Yup.string().required("Password is required"),
});

const Registration = () => {

  const router = useRouter();

  const {items} = useCart();
  const [submitAttempt, setsubmitAttempt] = useState<boolean>(false);

  const [passwordShown, setPasswordShown] = useState(false);
  const [loading, setLoading] = useState(false);

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  const handleData = async (formData: any) => {
    setLoading(true);
    try {

      console.log(items,"items");
      let regData: any
      if(items) {
        regData = await registerData({...formData, cartItems: items});
        
      } else {
        regData = await registerData(formData);
      }
      
      // let regData: any = await registerData(formData);
      
      toastAlert("success", regData.data.msg);
      router.push("/Login")
      setLoading(false);
      // reset();
    } catch (error) {
      setLoading(false);
      handleServerError(error);
    }

    // reset();
  };

  return (
    <>
      <div>
        <ToastContainer
          position="top-center"
          autoClose={1500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
      <div className="items-center justify-center gap-6 rounded-lg p-8 md:grid lg:grid-cols-3 xl:grid-cols-3 py-10 ">
        <div className="col-start-2">
          <div className="rounded-lg border bg-card text-card-foreground shadow max-w-md m-auto">
            <div className="flex flex-col p-6 space-y-1">
              <h3 className="font-semibold tracking-tight text-4xl text-center py-10">
                Registration
              </h3>

              <RegistrationForm
                submitAttempt={submitAttempt}
                setsubmitAttempt={setsubmitAttempt}
                passwordShown={passwordShown}
                setPasswordShown={setPasswordShown}
                schema={schema}
                togglePassword={togglePassword}
                loading={loading}
                setLoading={setLoading}
                handleData= {handleData}
              />

              <div className="flex justify-between"></div>
              {/* <div className="basis-1/4 flex flex-col items-center justify-center py-5">
                <Button type="submit">Submit</Button>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Registration;
