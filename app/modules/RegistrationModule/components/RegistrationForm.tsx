import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Eye, EyeOff } from "lucide-react";
import { handleServerError } from "@/app/utils/helpers";
import { registerData } from "@/app/api/login";
import { toastAlert } from "@/app/shared/components/Layout";
// import { auth, googleProvider } from "@/app/utils/firebase";
import {
  createUserWithEmailAndPassword,
  signInWithPopup,
  signOut,
} from "firebase/auth";
import { signIn, useSession } from "next-auth/react";
import Google from "@/app/icons/Google";
import Apple from "@/app/icons/Apple";
import FaceBook from "@/app/icons/FaceBook";
import { Formik } from "formik";
import { useRouter } from "next/router";



export const initValues: any = {
  name: "",
  email: "",
  mobile: "",
  password: "",
};

function RegistrationForm({
  submitAttempt,
  setsubmitAttempt,
  passwordShown,
  setPasswordShown,
  schema,
  togglePassword,
  loading,
  setLoading,
  handleData,
}: any) {

  const router = useRouter();

  
  const backToLogin = () => {
    router.push(`/Login`);
  };

  // const signInWithGoogle = async () => {
  //   try {
  //     await signInWithPopup(auth, googleProvider);
  //   } catch (err) {
  //     console.error(err);
  //   }
  // };
  return (
    <>
      <Formik
        initialValues={initValues}
        validationSchema={schema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleData(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
        }) => (
          <>
            <div>
              <Input
                //@ts-ignore
                label="Name"
                required
                onChange={handleChange}
                value={values.name}
                name="name"
                error={submitAttempt && errors.name}
                onBlur={handleBlur}
              />

              <Input
                //@ts-ignore
                label="Email"
                type="email"
                isRequired
                placeholder="Enter email"
                onChange={handleChange}
                value={values.email}
                name="email"
                error={submitAttempt && errors.email}
                onBlur={handleBlur}
              />

              <Input
                //@ts-ignore
                label="Mobile No."
                type="text"
                placeholder="Enter Mobile No."
                onChange={handleChange}
                value={values.mobile}
                name="mobile"
                onBlur={handleBlur}
              />

              <div className="relative">
                <Input
                  //@ts-ignore
                  label="Password"
                  type={passwordShown ? "text" : "password"}
                  placeholder="Enter Password"
                  isRequired
                  onChange={handleChange}
                  value={values.password}
                  name="password"
                  error={submitAttempt && errors.password}
                  onBlur={handleBlur}
                />
                <Button
                  variant="outline"
                  size="icon"
                  className="absolute inset-y-6 right-0"
                  onClick={togglePassword}
                >
                  {passwordShown ? (
                    <Eye className="h-4 w-4" />
                  ) : (
                    <EyeOff className="h-4 w-4" />
                  )}
                </Button>
              </div>
            </div>

            <div className="basis-1/2 flex flex-row items-center justify-between">
              <Button variant="link" onClick={backToLogin}>
                Back To Log-In
              </Button>

              {/* {loading ? (
            <div
              className="loading"
              style={{ position: "relative", transform: "scale(0.4)" }}
            ></div>
          ) : ( */}
              <Button
                type="submit"
                className="bottom-0"
                onClick={(e: any) => {
                  handleSubmit(e);
                  setsubmitAttempt(true);
                }}
              >
                Submit
              </Button>
              {/* )} */}
            </div>
            <div className="relative flex flex-row justify-center my-5">
              <span className="bg-white px-2 relative z-10">or</span>
              <div className="h-px w-3/4 bg-gray-300 absolute z-0 inset-y-3"></div>
            </div>
            <div className="flex flex-row justify-center">
              <Button
                onClick={() =>
                  signIn("apple", {
                    callbackUrl: "https://b556-49-43-33-220.ngrok-free.app/",
                  })
                }
                className="w-1/3 text-white bg-gray-800 hover:bg-gray-900 rounded-lg p-3 bottom-0 mx-1"
              >
                <Apple size={20} color={"currentColor"} />
              </Button>
              <Button
                onClick={() => signIn("facebook", { callbackUrl: "/" })}
                className="w-1/3 text-white bg-blue-700 hover:bg-blue-800 rounded-lg p-3 bottom-0 mx-1"
              >
                <FaceBook size={20} color={"currentColor"} />
              </Button>
              <Button
                onClick={() => signIn("google", { callbackUrl: "/" })}
                // type="submit"
                className="w-1/3 text-white bg-red-700 hover:bg-red-800 rounded-lg p-3 bottom-0 mx-1"
              >
                <Google size={20} color={"currentColor"} />
              </Button>
            </div>
          </>
        )}
      </Formik>

      {/* // <form onSubmit={handleSubmit(onSubmitHandler)}>
      //   <Input
      //     label="Name"
      //     type="text"
      //     isRequired
      //     placeholder=""
      //     {...register("name")}
      //     error={errors.name?.message}
      //   />

      //   <Input
      //     label="Email"
      //     type="email"
      //     isRequired
      //     placeholder="Enter email"
      //     {...register("email")}
      //     error={errors.email?.message}
      //   />

      //   <Input
      //     label="Mobile No."
      //     type="text"
      //     placeholder="Enter Mobile No."
      //     {...register("mobile")}
      //   />

      //   <div className="relative">
      //     <Input
      //       label="Password"
      //       type={passwordShown ? "text" : "password"}
      //       placeholder="Enter Password"
      //       isRequired
      //       {...register("password")}
      //       error={errors.password?.message}
      //     />
      //     <Button
      //       variant="outline"
      //       size="icon"
      //       className="absolute inset-y-6 right-0"
      //       onClick={togglePassword}
      //     >
      //       {passwordShown ? (
      //         <Eye className="h-4 w-4" />
      //       ) : (
      //         <EyeOff className="h-4 w-4" />
      //       )}
      //     </Button>
      //   </div>

      //   <div className="basis-1/2 flex flex-row items-center justify-between">
      //     <Button variant="link" onClick={backToLogin}>
      //       Back To Log-In
      //     </Button>
      //     <Button type="submit" className="bottom-0">
      //       Submit
      //     </Button>
      //   </div>
      //   <div className="relative flex flex-row justify-center my-5">
      //     <span className="bg-white px-2 relative z-10">or</span>
      //     <div className="h-px w-3/4 bg-gray-300 absolute z-0 inset-y-3"></div>
      //   </div>
      //   <div className="flex flex-row justify-center">
      //     <Button
      //       onClick={() =>
      //         signIn("apple", {
      //           callbackUrl: "https://b556-49-43-33-220.ngrok-free.app/",
      //         })
      //       }
      //       className="w-1/3 text-white bg-gray-800 hover:bg-gray-900 rounded-lg p-3 bottom-0 mx-1"
      //     >
      //       <Apple size={20} color={"currentColor"} />
      //     </Button>
      //     <Button
      //       onClick={() => signIn("facebook", { callbackUrl: "/" })}
      //       className="w-1/3 text-white bg-blue-700 hover:bg-blue-800 rounded-lg p-3 bottom-0 mx-1"
      //     >
      //       <FaceBook size={20} color={"currentColor"} />
      //     </Button>
      //     <Button
      //       onClick={() => signIn("google", { callbackUrl: "/" })}
      //       className="w-1/3 text-white bg-red-700 hover:bg-red-800 rounded-lg p-3 bottom-0 mx-1"
      //     >
      //       <Google size={20} color={"currentColor"} />
      //     </Button>
      //   </div>
      // </form> */}
    </>
  );
}

export default RegistrationForm;
