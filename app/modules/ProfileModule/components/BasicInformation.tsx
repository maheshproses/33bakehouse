import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Formik } from "formik";
import type { NextPage } from "next";
import { useState } from "react";

export const initValues: any = {
  name: "",
  email: "",
  mobile: "",
};

function BasicInformation ({ profileSchema, handleProfileData, profileData }: any)  {
  const [submitAttempt, setsubmitAttempt] = useState<boolean>(false);
  const [loading, setLoading] = useState(false);

  return (
    <>
      <div className="grid gap-2 ">
        <div>
          <p className="text-base font-bold">Basic Information</p>
        </div>
        {/* <div className="grid gap-2 sm:grid-cols-2 lg:grid-cols-2">
          <div className="flex flex-col space-y-1.5">
            <Label htmlFor="name">Name</Label>
            <Input type="name" id="name" placeholder="Name" />
          </div>
          <div className="flex flex-col space-y-1.5">
            <Label htmlFor="phonenumber">Phone Number</Label>
            <Input type="phone" id="phone" placeholder="9979501130" />
          </div>
          <div className="flex flex-col space-y-1.5">
            <Label htmlFor="email">Email</Label>
            <Input type="email" id="email" placeholder="Email" />
          </div>
        </div> */}
        {/* <div className="flex justify-end">
              <Button>Save</Button>
            </div> */}

        <Formik
          initialValues={profileData}
          validationSchema={profileSchema}
          enableReinitialize
          onSubmit={(values, { resetForm }) => {
            handleProfileData(values);
            // resetForm();
          }}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue,
          }) => (
            <>
             <div className="grid gap-2 sm:grid-cols-2 lg:grid-cols-2">
                <Input
                  label="Name"
                  required
                  onChange={handleChange}
                  value={values.name}
                  name="name"
                  error={submitAttempt && errors.name}
                  onBlur={handleBlur}
                />

                <Input
                  label="Email"
                  type="email"
                  isRequired
                  placeholder="Enter email"
                  onChange={handleChange}
                  value={values.email}
                  name="email"
                  error={submitAttempt && errors.email}
                  onBlur={handleBlur}
                />

                <Input
                  label="Mobile No."
                  type="text"
                  placeholder="Enter Mobile No."
                  onChange={handleChange}
                  value={values.mobile}
                  name="mobile"
                  onBlur={handleBlur}
                />

              </div>

              <div className="basis-1/2 flex flex-row items-center justify-end text-right">

                <Button
                  type="submit"
                  className="bottom-0"
                  onClick={(e: any) => {
                    handleSubmit(e);
                    setsubmitAttempt(true);
                  }}
                >
                  Save
                </Button>
              </div>
            </>
          )}
        </Formik>
      </div>
    </>
  );
};

export default BasicInformation;
