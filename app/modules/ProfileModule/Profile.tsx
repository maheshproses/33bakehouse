import type { NextPage } from "next";
import PageTitle from "../ProductsModule/components/PageTitle/PageTitle";
import BasicInformation from "./components/BasicInformation";
import { useContext, useEffect, useState } from "react";
import * as Yup from "yup";
import { handleServerError } from "@/app/utils/helpers";
import {
  deleteUserAddress,
  getProfileData,
  getUserAddressList,
  updateProfile,
  updateUserAddress,
} from "@/app/api/apis";
import AuthContext from "@/app/contexts/auth/auth.context";
import AddressForm from "../CartModule/components/AddressForm";
import AddressList from "../CartModule/components/AddressList";
import { toastAlert } from "@/app/shared/components/Layout";
import { ADDRESS_TYPE } from "@/app/utils/constants";
import { Button } from "@/components/ui/button";
import { PlusCircle } from "lucide-react";

const profileSchema = Yup.object().shape({
  name: Yup.string().max(255).required("Name is required"),
  mobile: Yup.string()
    .matches(
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      "Phone number is not valid"
    )
    .typeError("Please Enter Valid Phone number"),
  email: Yup.string()
    .required("Email is required")
    .email("Please Enter Valid Email")
    .matches(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      "Please Enter Valid Email"
    ),
});

const Profile: NextPage = () => {
  const { authState }: any = useContext(AuthContext);

  const [profileData, setProfileData] = useState<any>({});
  const [showAddress, setShowAddress] = useState<boolean>(false);
  const [addressList, setAddressList] = useState<any>([]);
  const [addressFormData, setAddressFormData] = useState<any>([]);
  const [btnLoader, setBtnLoader] = useState<boolean>(false);
  const [addId, setAddID] = useState<number>();

  useEffect(() => {
    if (authState.authenticated) {
      getProfile();
      getUserAddress();
    } else {
      // router.push("/Login");
    }

    return () => {};
  }, []);

  // get cart list
  const getProfile = async () => {
    try {
      let profile = await getProfileData();
      setProfileData(profile.data.data);
    } catch (error: any) {
      handleServerError(error);
    }
  };

  // get customer address list
  const getUserAddress = async () => {
    try {
      //@ts-ignore
      let userId = JSON.parse(localStorage.getItem("user_data")).id;

      let addressList = await getUserAddressList(userId);

      setAddressList(addressList.data.data);
    } catch (error: any) {
      handleServerError(error);
    }
  };

  const handleProfileData = async (formValues: any) => {
    try {
      
      // //@ts-ignore
      // let userId: any = JSON.parse(localStorage.getItem("user_data")).id;
      console.log(formValues, "formValues");
      // return;
      let updprofileData: any = await updateProfile(formValues.id, formValues);
      console.log(updprofileData, "updprofileData");

      toastAlert("success", updprofileData.data.msg);
    } catch (error: any) {
      console.log(error);
      handleServerError(error);
    }
  };

  // show address form
  const showAddressFrom = () => {
    setShowAddress(true);
  };

  // hide address form
  const hideAddressFrom = () => {
    setShowAddress(false);
    setAddressFormData([]);
    setAddID(undefined);
  };

  //delete customer address api
  const deleteaddress = async (id: number) => {
    try {
      let deleteData = await deleteUserAddress(id);
      getUserAddress();

      toastAlert("success", deleteData.data.msg);
    } catch (error: any) {
      console.log(error);
      handleServerError(error);
    }
  };

  //update cussstomer address api
  const updateaddress = async (formValues: any) => {
    try {
      
      setBtnLoader(true);
      let updateData = await updateUserAddress(addId, formValues);
      getUserAddress();
      setAddressFormData([]);
      setAddID(undefined);
      setShowAddress(false);
      toastAlert("success", updateData.data.msg);
      setBtnLoader(false);
    } catch (error: any) {
      setBtnLoader(false);
      console.log(error);
      handleServerError(error);
    }
  };

  // update function
  const editinput = async (addressObj: any) => {
    try {
      
      if (addressObj.id) {
        setAddID(addressObj.id);
      }
      let editData = {
        title: addressObj.title,
        address: addressObj.address,
        cityName: addressObj.cityName,
        pincode: addressObj.pincode,
      };
      setAddressFormData(editData);
      // setCustomerInfo(editData)
      showAddressFrom();
    } catch (error: any) {
      console.log(error);
      handleServerError(error);
    }
  };

  return (
    <>
      <PageTitle pageTitle="Profile" />
      <div className="max-w-xl mx-auto xl:block m-5">
        <div className="rounded-lg border bg-card text-card-foreground shadow-sm p-5">
          <BasicInformation
            profileSchema={profileSchema}
            handleProfileData={handleProfileData}
            profileData={profileData}
          />
          <div className="border-collapse border-spacing-1 border-y border-gray-200 py-5 mt-5">
            <div className="flex justify-between h-10">
              {!showAddress ? (
                <>
                  <div className="justify-items-start">
                    <p className="text-base font-bold">Address List</p>
                  </div>
                  <div className="justify-items-end">
                    <Button
                      onClick={showAddressFrom}
                      size="sm"
                      className="text-end"
                    >
                      <PlusCircle className="mr-2 h-4 w-4" /> Add New Address
                    </Button>
                  </div>
                </>
              ) : (
                <div className="justify-items-start">
                  <p className="text-base font-bold">Add Address</p>
                </div>
              )}
            </div>

            {/* <AddAddress /> */}

            {showAddress ? (
              <AddressForm
                hideAddressFrom={hideAddressFrom}
                addId={addId}
                updateaddress={updateaddress}
                addressFormData={addressFormData}
                getUserAddress={getUserAddress}
                setBtnLoader={setBtnLoader}
                btnLoader={btnLoader}
              />
            ) : null}
          </div>
          {/* <div className="border-b-2 py-5"></div> */}
          {/* <AddressList
            showAddress={showAddress}
            showAddressFrom={showAddressFrom}
          /> */}
          {!showAddress ? (
            <AddressList
              addressList={addressList}
              changeAddress={() => {}}
              type={ADDRESS_TYPE.BILLING}
              action={true}
              deleteaddress={deleteaddress}
              editinput={editinput}
            />
          ) : null}
        </div>
      </div>
      {/* <div className="container mx-auto space-y-10">
        <button className="px-10 py-5 bg-sky-300 hover:bg-amber-500 peer">
          <div
            className="p-10 text-2xl border border-slate-500 peer-hover:bg-teal-700 peer-hover:text-4xl peer-hover:text-white">
            Linuxhint.com
          </div>
          <Label htmlFor="email">Email</Label>
                <Input type="email" id="email" placeholder="Email" className="peer-hover:bg-teal-700 peer-hover:text-white" />
        </button>
      </div> */}
    </>
  );
};

export default Profile;
