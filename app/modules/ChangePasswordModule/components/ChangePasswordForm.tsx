import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Formik } from "formik";
import { Eye, EyeOff } from "lucide-react";
import React from "react";

export const initValues: any = {
  oldPassword: "",
  newPassword: "",
  ConfirmPassword: "",
};

function ChangePasswordForm({ changePasswordSchema, passwordShown1, passwordShown2, passwordShown3, togglePassword1, togglePassword2, togglePassword3, handleData }: any) {
  return (
    <>
      <Formik
        initialValues={initValues}
        validationSchema={changePasswordSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          handleData(values);
          // resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
          resetForm,
        }) => (
          <>
              <div className="relative">
              <Input
                label="Old Password"
                type={passwordShown1 ? "text" : "oldPassword"}
                isRequired
                placeholder="Enter password"
                onChange={handleChange}
                value={values.oldPassword}
                name="oldPassword"
                error={errors.oldPassword}
              />
              <Button
                variant="outline"
                size="icon"
                className="absolute inset-y-6 right-0"
                onClick={togglePassword1}
              >
                {passwordShown1 ? (
                  <Eye className="h-4 w-4" />
                ) : (
                  <EyeOff className="h-4 w-4" />
                )}
              </Button>
            </div>

            <div className="relative">
              <Input
                label="Password"
                type={passwordShown2 ? "text" : "password"}
                isRequired
                placeholder="Enter new password"
                onChange={handleChange}
                value={values.newPassword}
                name="newPassword"
                error={errors.newPassword}
              />
              <Button
                variant="outline"
                size="icon"
                className="absolute inset-y-6 right-0"
                onClick={togglePassword2}
              >
                {passwordShown2 ? (
                  <Eye className="h-4 w-4" />
                ) : (
                  <EyeOff className="h-4 w-4" />
                )}
              </Button>
            </div>

            <div className="relative">
              <Input
                label="Password"
                type={passwordShown3 ? "text" : "password"}
                isRequired
                placeholder="Enter confirm password"
                onChange={handleChange}
                value={values.ConfirmPassword}
                name="ConfirmPassword"
                error={errors.ConfirmPassword}
              />
              <Button
                variant="outline"
                size="icon"
                className="absolute inset-y-6 right-0"
                onClick={togglePassword3}
              >
                {passwordShown3 ? (
                  <Eye className="h-4 w-4" />
                ) : (
                  <EyeOff className="h-4 w-4" />
                )}
              </Button>
            </div>
            <div className="flex flex-row items-center justify-between mb-4">
              <Button type="submit" onClick={() => handleSubmit()}>
                Submit
              </Button>
            </div>

          </>
        )}
      </Formik>
    </>
  );
}

export default ChangePasswordForm;
