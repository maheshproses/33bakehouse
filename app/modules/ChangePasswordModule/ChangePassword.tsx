import React, { useState } from "react";
import ChangePasswordForm from "./components/ChangePasswordForm";
import * as Yup from "yup";
import { handleServerError } from "@/app/utils/helpers";
import { toastAlert } from "@/app/shared/components/Layout";
import { updatePassword } from "@/app/api/login";
import { useRouter } from "next/router";

const changePasswordSchema = Yup.object().shape({
  oldPassword: Yup.string().required("Password is required"),
  // oldPassword: Yup.string().required("Password is required"),
  newPassword: Yup.string().required("Password is required"),
  // .min(8, "Password should have atleast 8 characters."),
  ConfirmPassword: Yup.string()
    .required("Password is required")
    .oneOf([Yup.ref("newPassword")], "Both password need to be the same"),
  // .when("newPassword", {
  //   is: (val: any) => (val && val.length > 0 ? true : false),
  //   then: Yup.string().oneOf(
  //     [Yup.ref("newPassword")],
  //     "Both password need to be the same"
  //   )
  // })
});

function ChangePassword() {
  const router = useRouter();
  const [passwordShown1, setPasswordShown1] = useState(false);
  const [passwordShown2, setPasswordShown2] = useState(false);
  const [passwordShown3, setPasswordShown3] = useState(false);

  const togglePassword1 = () => {
    setPasswordShown1(!passwordShown1);
  };

  const togglePassword2 = () => {
    setPasswordShown2(!passwordShown2);
  };

  const togglePassword3 = () => {
    setPasswordShown3(!passwordShown3);
  };

  const handleData = async (formValues: any) => {
    try {
      console.log(formValues, "formValues");

      let result = await updatePassword(formValues);

      toastAlert("success", result.data.msg);
      console.log(result);

      router.push("/");
    } catch (error: any) {
      console.log(error);
      handleServerError(error);
    }
  };

  return (
    <div className="items-center justify-center gap-6 rounded-md p-8 md:grid lg:grid-cols-2 xl:grid-cols-3 py-10 ">
      <div className="col-start-2 rounded-md border bg-background shadow">
        <div className="rounded-lg border bg-card text-card-foreground shadow-sm">
          <div className="flex flex-col p-6 space-y-1">
            <h3 className="font-semibold tracking-tight text-4xl text-center py-10">
              Change Password
            </h3>
            <div>
              <ChangePasswordForm
                changePasswordSchema={changePasswordSchema}
                passwordShown1={passwordShown1}
                passwordShown2={passwordShown2}
                passwordShown3={passwordShown3}
                togglePassword1={togglePassword1}
                togglePassword2={togglePassword2}
                togglePassword3={togglePassword3}
                handleData={handleData}
              />
            </div>
            {/* <div className="grid w-full max-w-sm items-center gap-1.5">
            <Label htmlFor="password">Password</Label>
            <Input type="password" id="password" placeholder="Password" />
          </div>
          <div className="grid w-full max-w-sm items-center gap-1.5">
            <Label htmlFor="Confirm password">Confirm password</Label>
            <Input
              type="Confirm password"
              id="Confirm password"
              placeholder="Password"
            />
          </div>
          <Button className="w-1/3 text-white bg-gray-800 hover:bg-gray-900 rounded-lg p-3 bottom-0 mx-1">
            Reset Password
          </Button> */}
          </div>
        </div>
      </div>
    </div>

    // <div>
    // </div>
  );
}

export default ChangePassword;
