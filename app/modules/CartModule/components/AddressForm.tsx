import { addUserAddress } from "@/app/api/apis";
import { toastAlert } from "@/app/shared/components/Layout";
import SelectMenu from "@/components/ui/Select/Select";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { Formik } from "formik";
import { useState } from "react";
import * as Yup from "yup";

const ValidationSchema = Yup.object().shape({
  title: Yup.string().required("Title is required"),
  address: Yup.string().required("Address is required"),
  cityName: Yup.string().required("City is required"),
  pincode: Yup.number()
    .required("Pincode is required")
    .typeError("Pincode must be a number"),
});

export const initialState: any = {
  title: "",
  address: "",
  cityName: "",
  pincode: "",
};

const CITY_LIST = [
  { id: "Dubai", value: "Dubai" },
  { id: "Abudhabi", value: "Abudhabi" },
  { id: "Sharjah", value: "Sharjah" },
];

function AddressForm({
  hideAddressFrom,
  setBtnLoader,
  btnLoader,
  getUserAddress,
  addId,
  addressFormData,
  updateaddress,
}: any) {
  const [submitAttempt, setsubmitAttempt] = useState<boolean>(false);

  const handleData = async (formValues: any) => {
    try {
      //@ts-ignore
      let userId = JSON.parse(localStorage.getItem("user_data")).id;
      setBtnLoader(true);
      let res = await addUserAddress({ ...formValues, userId });
      toastAlert("success", res.data.msg);
      setBtnLoader(false);
      hideAddressFrom();
      getUserAddress();
    } catch (error) {
      setBtnLoader(false);
      toastAlert("error", "Somthing went wrong!");
    }
  };

  return (
    <>
      <Formik
        // initialValues={addressFormData ? addressFormData : initialState}
        initialValues={addId ? addressFormData : initialState}
        validationSchema={ValidationSchema}
        enableReinitialize
        onSubmit={(values, { resetForm }) => {
          {
            addId ? updateaddress(values) : handleData(values);
          }
          return;

          // handleData(values);
          //   updateaddress (values)
          // setAddAddressForm(values);
          resetForm();
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched,
          setFieldValue,
        }) => (
          <>
            <Input
              label="Title"
              floatLabel
              value={values.title}
              onChange={handleChange}
              name="title"
              error={submitAttempt && errors.title}
              isRequired
              placeholder=""
            />

            <Textarea
              label="Address"
              floatLabel
              name="address"
              value={values.address}
              onChange={handleChange}
              error={submitAttempt && errors.address}
              placeholder=""
            />

            {/* <Input
              label="City"
              floatLabel
              value={values.cityName}
              onChange={handleChange}
              name="cityName"
              isRequired
              error={submitAttempt && errors.cityName}
              placeholder=""
            /> */}

            <SelectMenu
              label="City"
              onChange={(e: any) => {
                setFieldValue("cityName", e.id);
              }}
              items={CITY_LIST}
              bindValue={"id"}
              bindName={"value"}
              value={values.cityName}
              name="cityName"
              isRequired
            />

            <Input
              label="Pincode"
              floatLabel
              name="pincode"
              value={values.pincode}
              onChange={handleChange}
              error={submitAttempt && errors.pincode}
              placeholder=""
            />

            <div className="text-end">
              {/* {addId ? (
              <> */}
              {/* // <Button color="gradient" style={{ marginRight: "10px" }} rounded onClick={updateaddress} align="right">Update</Button> */}
              {/* <Button
                  color="default"
                  style={{ marginRight: "10px" }}
                  rounded
                  onClick={(e: any) => {
                    handleSubmit(e);
                    setsubmitAttempt(true);
                  }}
                  align="right"
                  loading={btnLoader}
                  disabled={btnLoader}
                >
                  Update
                </Button>
              </>
            ) : ( */}

              {addId ? (
                <>
                  <Button
                    type="submit"
                    // className="h-10 px-5 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-full focus:shadow-outline hover:bg-indigo-800"
                    size="sm"
                    onClick={(e: any) => {
                      handleSubmit(e);
                      setsubmitAttempt(true);
                    }}
                  >
                    Update
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    type="submit"
                    // className="h-10 px-5 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-full focus:shadow-outline hover:bg-indigo-800"
                    size="sm"
                    onClick={(e: any) => {
                      handleSubmit(e);
                      setsubmitAttempt(true);
                    }}
                  >
                    Save
                  </Button>
                </>
              )}

              {/* )} */}

              <Button
                className="ms-2"
                // className="h-10 px-5 ms-2 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-full focus:shadow-outline hover:bg-indigo-800"
                size="sm"
                onClick={hideAddressFrom}
              >
                Cancel
              </Button>
            </div>
          </>
        )}
      </Formik>
    </>
  );
}

export default AddressForm;
