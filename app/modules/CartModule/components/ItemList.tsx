import { NODE_API_URL } from "@/app/utils/constants";

function ItemList({ cartList, subTotal, deliveryCharge, totalAmount }: any) {
  return (
    <div className="relative bg-[#F5F5F5]">
      <div className="px-4 py-5 sticky top-[100px]">
        {cartList.length > 0 ? (
          <div className="space-y-3 px-2 py-4 ">
            {cartList?.map((item: any) => {
              return (
                <div
                  className="flex flex-col justify-center items-center rounded-lg sm:flex-row py-1"
                  key={item.id}
                >
                  <div className="relative ">
                    {/* <figure>
       <img src="/cake.jpg" className="w-[148px]"/>
     </figure> */}
                    <img
                      src={`${
                        item?.Item?.itemImages[0]?.image
                          ? `${NODE_API_URL}/static/itemImageFile/${item?.Item?.itemImages[0]?.image}`
                          : `${NODE_API_URL}/static/itemImageFile/${item?.itemImage}`
                      }`}
                      className="w-16 md:w-32 max-w-full max-h-full"
                      // alt="Apple Watch"
                    />
                    {/* <Image
             src={`${
               item?.Item?.itemImages[0]?.image
                 ? `${NODE_API_URL}/static/itemImageFile/${item?.Item?.itemImages[0]?.image}`
                 : "no_image.png"
             }`}
             width={150}
             height={150}
             alt="Picture of the author"
             className="w-28 h-16 rounded-md border object-cover object-center"
           /> */}
                    <div className="absolute inline-flex items-center  justify-center w-5 h-5 text-xs font-medium text-white bg-gray-500 b rounded-full -top-2 -right-3 dark:border-gray-900">
                      {item.quantity}
                    </div>
                  </div>
                  <div className="flex w-full flex-col px-8 ">
                    <span className="font-semibold">
                      {item?.Item?.itemName ? item?.Item?.itemName : item?.name}
                    </span>
                    {/* <p className="text-xs text-slate-400">{item.description}</p> */}
                  </div>
                  <div className="flex items-center px-2">
                    <p className="text-sm font-bold">AED</p>
                    <p className="text-sm font-bold ml-0.5"> {item.amount}</p>
                  </div>
                </div>
              );
            })}
            {/* <div className="flex flex-col rounded-lg sm:flex-row">
 <img
   className="m-2 h-20 w-20 rounded-md border object-cover object-center"
   src="./cake.jpg"
   alt=""
 />
 <div className="flex w-full flex-col px-4">
   <span className="font-semibold">Cake</span>
   <p className="text-xs text-slate-400">It is a long established fact that a reader will be distracted by the readable distracted by the readable distracted by the readable</p>
  
 </div>
 <div className="flex items-center px-4 py-4">
     AED
     <p className="text-lg font-bold">138.99</p>
   </div>
</div> */}
          </div>
        ) : (
          <p className="text-center">
            <strong>Cart Is Empty</strong>
          </p>
        )}

        {/* <div className="md:flex sm:flex-row md:max-w-full gap-2">
          <div className="lg:w-5/6 md:max-w-full sm:max-w-full sm:text-lg">
            <Input
              type="text"
              id="nameoncard"
              placeholder="Discount code or gift card"
            />
          </div>
          <div className="sm:max-w-sm lg:w-1/6 md:max-w-md">
            <Button className="bg-gray-400 w-full">Apply</Button>
          </div>
        </div> */}

        <div className="py-2">
          <div className="flex items-center justify-between">
            <p className="text-sm font-normal text-gray-900">Subtotal</p>
            <div className="flex items-center  px-2">
              <p className="text-sm font-bold">AED</p>
              <p className="text-sm font-bold ml-0.5">{subTotal}</p>
            </div>
          </div>
          <div className="flex items-center justify-between py-2">
            <p className="text-sm font-normal text-gray-900">Delivery charge</p>

            <div className="flex items-center  px-2">
              <p className="text-sm font-bold">AED</p>
              <p className="text-sm font-bold ml-0.5">
                {deliveryCharge ? deliveryCharge : 0}
              </p>
            </div>
          </div>
          {/* <div className="flex items-center justify-between">
            <div className="flex justify-start">
              <p className="text-sm font-normal text-gray-900 mr-2">
                Estimated taxes
              </p>
              <p>
                <TooltipProvider>
                  <Tooltip>
                    <TooltipTrigger>
                      <HelpCircle size={15} className="text-gray-500" />
                    </TooltipTrigger>
                    <TooltipContent>Add Content here</TooltipContent>
                  </Tooltip>
                </TooltipProvider>
              </p>
            </div>
            <div className="flex items-center  px-2">
              <p className="text-sm font-bold">AED</p>
              <p className="text-sm font-bold ml-0.5">11.25</p>
            </div>
          </div> */}
        </div>
        <div className="mt-2 flex items-center justify-between">
          <p className="text-lg font-medium text-gray-900">Total</p>
          <div className="flex items-center  px-2">
            <p className="text-lg font-bold">AED</p>
            <p className="text-md font-bold ml-0.5">
              {totalAmount ? totalAmount : subTotal}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ItemList;
