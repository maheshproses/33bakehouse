import React, { useState } from "react";

function PaymentType({ changePaymentType, paymentType }: any) {
  return (
    <>
      <div>
        <p>
          <strong>Payment Type</strong>
        </p>
      </div>
      <div className="mb-9">
        <div className="relative" onClick={(e: any) => changePaymentType(e)}>
          <input
            type="radio"
            id={"CashOnDelivery"}
            value={"CashOnDelivery"}
            checked={paymentType === "CashOnDelivery"}
            name={"paymentType"}
            className="absolute inset-y-0 left-5 peer w-5"
          />
          <label
            htmlFor={"CashOnDelivery"}
            className="flex items-center w-full p-5 pl-14 text-gray-500 bg-white border rounded-lg rounded-b-none cursor-pointer transition-all
      peer-checked:border peer-checked:rounded-b-none peer-checked:border-indigo-500  hover:text-gray-600 hover:bg-gray-100 peer-checked:bg-blue-50 peer-checked:focus:ring-8"
          >
            <div className="flex w-full justify-between items-center">
              <div className="flex items-center justify-start space-x-2">
                <div className="flex flex-col">
                  <p className="text-gray-700 text-sm pb-0 font-medium leading-0">
                    Cash On Delivery
                  </p>
                </div>
              </div>
            </div>
          </label>
        </div>

        <div className="relative" onClick={(e: any) => changePaymentType(e)}>
          <input
            type="radio"
            id={"OnlinePayment"}
            value={"OnlinePayment"}
            checked={paymentType === "OnlinePayment"}
            name={"paymentType"}
            className="absolute inset-y-0 left-5 peer w-5"
          />
          <label
            htmlFor={"OnlinePayment"}
            className="flex items-center w-full p-5 pl-14 text-gray-500 bg-white border rounded-lg rounded-b-none cursor-pointer transition-all
      peer-checked:border peer-checked:rounded-b-none peer-checked:border-indigo-500  hover:text-gray-600 hover:bg-gray-100 peer-checked:bg-blue-50 peer-checked:focus:ring-8"
          >
            <div className="flex w-full justify-between items-center">
              <div className="flex items-center justify-start space-x-2">
                <div className="flex flex-col">
                  <p className="text-gray-700 text-sm pb-0 font-medium leading-0">
                    Online Payment
                  </p>
                </div>
              </div>
            </div>
          </label>
        </div>

        <div className="relative" onClick={(e: any) => changePaymentType(e)}>
          <input
            type="radio"
            id={"OfflineBank"}
            value={"OfflineBank"}
            checked={paymentType === "OfflineBank"}
            name={"paymentType"}
            className="absolute inset-y-0 left-5 peer w-5"
          />
          <label
            htmlFor={"OfflineBank"}
            className="flex items-center w-full p-5 pl-14 text-gray-500 bg-white border rounded-lg rounded-b-none cursor-pointer transition-all
      peer-checked:border peer-checked:rounded-b-none peer-checked:border-indigo-500  hover:text-gray-600 hover:bg-gray-100 peer-checked:bg-blue-50 peer-checked:focus:ring-8"
          >
            <div className="flex w-full justify-between items-center">
              <div className="flex items-center justify-start space-x-2">
                <div className="flex flex-col">
                  <p className="text-gray-700 text-sm pb-0 font-medium leading-0">
                    Offline / Bank
                  </p>
                </div>
              </div>
            </div>
          </label>
        </div>
      </div>
    </>
  );
}

export default PaymentType;
