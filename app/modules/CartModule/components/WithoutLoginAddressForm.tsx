import React, { useState } from "react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Plus, Search } from "lucide-react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Textarea } from "@/components/ui/textarea";
import * as Yup from "yup";
import SelectMenu from "@/components/ui/Select/Select";
import { Formik } from "formik";
import { toastAlert } from "@/app/shared/components/Layout";
import { addUserAddress, addWithoutLoginPlaceOrder } from "@/app/api/apis";
import PaymentType from "./PaymentType";
import { addressObjToText } from "@/app/utils/helpers";
import { useCart } from "@/app/shared/hooks/use-cart/use-cart.hook";
import { useRouter } from "next/router";

const ValidationSchema = Yup.object().shape({
  name: Yup.string().required("Name is required"),
  mobile: Yup.string()
    .matches(
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      "Phone number is not valid"
    )
    .required("Phone number is required")
    .typeError("Please Enter Valid Phone number"),
  email: Yup.string()
    .required("Email is required")
    .email("Please Enter Valid Email")
    .matches(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      "Please Enter Valid Email"
    ),
  cityName: Yup.string().required("City is required"),
  pincode: Yup.number()
    .required("Pincode is required")
    .typeError("Pincode must be a number"),
});

export const initialState: any = {
  name: "",
  mobile: "",
  email: "",
  address: "",
  cityName: "",
  pincode: "",
};

const CITY_LIST = [
  { id: "Dubai", value: "Dubai" },
  { id: "Abudhabi", value: "Abudhabi" },
  { id: "Sharjah", value: "Sharjah" },
];

function WithoutLoginAddressForm({
  setBtnLoader,
  handlDeliveryCharge,
  changePaymentType,
  paymentType,
  subTotal,
  totalAmount,
  cartItems,
  deliveryCharge,
  setTotalAmount,
  setDeliveryCharge,
}: any) {
  const router = useRouter();
  const cart = useCart();
  const [submitAttempt, setsubmitAttempt] = useState<boolean>(false);

  const withoutLoginPlaceOrder = async (formValues: any) => {
    try {
      let cartIndex = 0;
      let cartDetails = [];
      while (cartIndex < cartItems.length) {
        cartDetails.push({
          ...cartItems[cartIndex],
          itemId: cartItems[cartIndex].itemId,
          QTY: cartItems[cartIndex].quantity,
          amount: cartItems[cartIndex].amount,
        });

        cartIndex++;
      }

      setBtnLoader(true);
      // return;

      let placeOrderDetail = {
        ...formValues,
        shippingAddress: formValues ? addressObjToText(formValues) : null,
        paymentType: paymentType,
        subTotal: subTotal,
        deliveryCharges: deliveryCharge,
        total: totalAmount ? totalAmount : subTotal,
        cartList: cartDetails,
        isInvoice: true,
      };

      let res = await addWithoutLoginPlaceOrder({ ...placeOrderDetail });
      console.log(res, "res");

      cart.clear();
      setTotalAmount(0);
      setDeliveryCharge(0);
      toastAlert("success", res.data.msg);
      router.push({
        pathname: "/TrackOrder",
        query: { OrderNo: `${res.data.data.orders.orderNo}` },
      });
      setBtnLoader(false);
    } catch (error) {
      setBtnLoader(false);
      toastAlert("error", "Somthing went wrong!");
    }
  };

  return (
    <Formik
      // initialValues={addressFormData ? addressFormData : initialState}
      initialValues={initialState}
      validationSchema={ValidationSchema}
      enableReinitialize
      onSubmit={(values, { resetForm }) => {
        console.log(values, "values");
        withoutLoginPlaceOrder(values);
        resetForm();
      }}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        errors,
        touched,
        setFieldValue,
      }) => (
        <>
          <Input
            label="Full Name"
            floatLabel
            value={values.name}
            onChange={handleChange}
            name="name"
            error={submitAttempt && errors.name}
            isRequired
            placeholder=""
          />

          <Input
            label="Mobile No."
            floatLabel
            value={values.mobile}
            onChange={handleChange}
            name="mobile"
            error={submitAttempt && errors.mobile}
            isRequired
            placeholder=""
          />

          <Input
            label="Email"
            floatLabel
            value={values.email}
            onChange={handleChange}
            name="email"
            error={submitAttempt && errors.email}
            isRequired
            placeholder=""
          />

          <Textarea
            label="Address"
            floatLabel
            name="address"
            value={values.address}
            onChange={handleChange}
            error={submitAttempt && errors.address}
            placeholder=""
          />

          <SelectMenu
            label="City"
            onChange={(e: any) => {
              setFieldValue("cityName", e.id);
              handlDeliveryCharge(e.id);
            }}
            items={CITY_LIST}
            bindValue={"id"}
            bindName={"value"}
            value={values.cityName}
            name="cityName"
            isRequired
          />

          <Input
            label="Pincode"
            floatLabel
            name="pincode"
            value={values.pincode}
            onChange={handleChange}
            error={submitAttempt && errors.pincode}
            placeholder=""
          />

          {/* <div className="text-end">
            <Button
              type="submit"
              className="h-10 px-5 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-full focus:shadow-outline hover:bg-indigo-800"
              size="sm"
              onClick={(e: any) => {
                handleSubmit(e);
                setsubmitAttempt(true);
              }}
            >
              Save
            </Button>
          </div> */}

          <PaymentType
            changePaymentType={changePaymentType}
            paymentType={paymentType}
          />

          {paymentType == "CashOnDelivery" || paymentType == "OfflineBank" ? (
            // <div>
            <Button
              type="submit"
              size={"lg"}
              className="bg-[#197AB9] h-14 text-lg"
              onClick={(e: any) => {
                handleSubmit(e);
                setsubmitAttempt(true);
              }}
              // onClick={withoutLoginPlaceOrder}
            >
              PLACE ORDER ( AED {totalAmount ? totalAmount : subTotal} )
            </Button>
          ) : (
            <Button
              type="submit"
              size={"lg"}
              className="bg-[#197AB9] h-14 text-lg"
              onClick={(e: any) => {
                handleSubmit(e);
                setsubmitAttempt(true);
              }}
              // onClick={withoutLoginPlaceOrder}
            >
              PAY NOW ( AED {totalAmount ? totalAmount : subTotal} )
            </Button>
          )}
          {/* </div> */}
        </>
      )}
    </Formik>
  );
}

export default WithoutLoginAddressForm;
