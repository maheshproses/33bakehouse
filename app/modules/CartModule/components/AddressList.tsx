import ConfirmBox from "@/components/ui/ConfirmBox/ConfirmBox";
import { Button } from "@/components/ui/button";
import { MapPin, Pencil, Trash2 } from "lucide-react";
import React from "react";

function AddressList({
  addressList,
  changeAddress,
  type,
  selectedAdderess,
  action,
  editinput,
  deleteaddress,
}: any) {
  return (
    <div className="mb-9">
      {addressList.map((singleAddress: any) => {
        return (
          <div
            className="relative mb-2"
            onClick={() => changeAddress(singleAddress, type)}
            key={singleAddress?.id}
          >
            {action ? (
              <MapPin className="absolute inset-y-0 left-3 peer w-8 mt-6" />
            ) : (
              <input
                type="radio"
                id={singleAddress?.id}
                value={singleAddress?.id}
                checked={selectedAdderess?.id == singleAddress?.id}
                name={type}
                className="absolute inset-y-0 left-5 peer w-5"
              />
            )}

            <label
              htmlFor={type}
              className="flex items-center w-full p-5 pl-14 text-gray-500 bg-white border rounded-lg rounded-b-none cursor-pointer transition-all
      peer-checked:border peer-checked:rounded-b-none peer-checked:border-indigo-500  hover:text-gray-600 hover:bg-gray-100 peer-checked:bg-blue-50 peer-checked:focus:ring-8"
            >
              <div className="flex w-full justify-between items-center">
                <div className="flex items-center justify-start space-x-2">
                  <div className="flex flex-col">
                    <p className="text-gray-700 text-sm pb-0 font-medium leading-0">
                      {singleAddress.title}
                    </p>
                    <p className="text-slate-500 text-sm leading-0">
                      {`${singleAddress.address}, ${singleAddress.cityName} - ${singleAddress.pincode}`}
                    </p>
                  </div>
                </div>
              </div>

              <div>
                {action ? (
                  <>
                    <div className="grid grid-cols-2 gap-2">
                      <div>
                        <Pencil
                          size={17}
                          onClick={() => editinput(singleAddress)}
                        />
                      </div>
                     <div>
                        <ConfirmBox
                          trigger={<Trash2 size={15} className="pointer" />}
                        >
                          <p className="text-center">Are you sure?</p>
                          <div className="text-center mt-2">
                            <Button
                              size="xs"
                              onClick={() => deleteaddress(singleAddress.id)}
                            >
                              Yes
                            </Button>
                          </div>
                        </ConfirmBox>
                      </div>
                    </div>
                  </>
                ) : null}
              </div>
            </label>
          </div>
        );
      })}
    </div>
  );
}

export default AddressList;
