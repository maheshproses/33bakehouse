import { deleteCart, getCartList, updateCart } from "@/app/api/apis";
import AuthContext from "@/app/contexts/auth/auth.context";
import CartCountContext from "@/app/contexts/cartCount/cartCount.context";
import Cross from "@/app/icons/Cross";
import { toastAlert } from "@/app/shared/components/Layout";
import { useCart } from "@/app/shared/hooks/use-cart/use-cart.hook";
import { Button } from "@/components/ui/button";
import { NODE_API_URL } from "@/app/utils/constants";
import { handleServerError } from "@/app/utils/helpers";
import { Minus, Plus, Trash2 } from "lucide-react";
import Link from "next/link";
import router from "next/router";
import { useContext, useEffect, useMemo, useState } from "react";
import PageTitle from "../../ProductsModule/components/PageTitle/PageTitle";
import ConfirmBox from "@/components/ui/ConfirmBox/ConfirmBox";

function CartItemList() {
  const cart = useCart();
  const { authState }: any = useContext(AuthContext);

  const { cartCountDispatch }: any = useContext(CartCountContext);

  const [cartList, setCartList] = useState<any>([]);
  const [deleteBox, setDeleteBox] = useState<boolean>(false);

  const user = authState.authenticated;

  useEffect(() => {
    if (authState.authenticated) {
      getCartDetails();
    }
    getWithOutLoginCartDetail();
    return () => {};
  }, []);

  // get cart list
  const getCartDetails = async () => {
    try {
      let cartlist = await getCartList();
      setCartList(cartlist.data.data);
    } catch (error: any) {
      handleServerError(error);
    }
  };

  const getWithOutLoginCartDetail = () => {
    let itemList = cart.items;
    setCartList(itemList);
  };

  const handleAddQty = async (
    currentQTY: number,
    index: number,
    itemInfo: any
  ) => {
    try {
      await updateCart(cartList[index].id, {
        quantity: Number(currentQTY + 1),
        amount: parseFloat(itemInfo?.Item.basicPrice) * Number(currentQTY + 1),
      });
      getCartDetails();
    } catch (error) {
      handleServerError(error);
    }
  };

  const handleMinusQty = async (
    currentQTY: number,
    index: number,
    itemInfo: any
  ) => {
    try {
      if (currentQTY > 1) {
        await updateCart(cartList[index].id, {
          quantity: Number(currentQTY - 1),
          amount:
            parseFloat(itemInfo?.Item.basicPrice) * Number(currentQTY - 1),
        });
        getCartDetails();
      }
    } catch (error) {
      handleServerError(error);
    }
  };

  // delete single cart data
  const deletecartItem = async (id: number) => {
    try {
      let deleteData = await deleteCart(id);
      getCartDetails();
      toastAlert("success", deleteData.data.msg);
      cartCountDispatch({ type: "REMOVE_ITEM" });
    } catch (error: any) {
      handleServerError(error);
    }
  };

  const showDeleteBox = (id: number) => {
    if (id) {
      setDeleteBox(true);
    }
  };

  const hideDeleteBox = () => {
    setDeleteBox(false);
  };

  const subTotal = useMemo(() => {
    let cartIndex = 0;
    let tempSubTotal = 0;
    if (authState.authenticated) {
      while (cartIndex < cartList.length) {
        tempSubTotal += cartList[cartIndex].amount;
        cartIndex++;
      }
    } else {
      while (cartIndex < cart.items.length) {
        tempSubTotal += cart.items[cartIndex].amount;
        cartIndex++;
      }
    }
    return tempSubTotal;
  }, [cartList, cart.items]);

  const checkoutCart = () => {
    router.push("/Cart");
  };

  return (
    <>
      <PageTitle pageTitle="Your Shopping Cart" />

      <section className="h-auto bg-gray-100 py-12 sm:py-16 lg:py-10">
        <div className="relative overflow-x-auto sm:rounded-lg mx-auto max-w-screen-lg">
          <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                {/* <th scope="col" className="px-16 py-3">
                  <span className="sr-only">Image</span>
                </th> */}
                <th scope="col" className="px-6 py-3">
                  Product
                </th>
                <th scope="col" className="px-6 py-3 text-center">
                  Qty
                </th>
                <th scope="col" className="px-6 py-3 text-end">
                  Price
                </th>
                <th scope="col" className="px-6 py-3 text-center">
                  Action
                </th>
              </tr>
            </thead>
            {cartList.length > 0 ? (
              <>
                {cartList.map((item: any, index: number) => {
                  return (
                    <tbody key={item?.id}>
                      <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        {/* <td className="p-4">
                          <img
                            src={`${
                              item?.Item?.itemImages[0]?.image
                                ? `${NODE_API_URL}/static/itemImageFile/${item?.Item?.itemImages[0]?.image}`
                                : `${NODE_API_URL}/static/itemImageFile/${item?.itemImage}`
                            }`}
                            className="w-16 md:w-32 max-w-full max-h-full"
                            // alt="Apple Watch"
                          />
                        </td> */}
                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white">
                        <div className="flex items-center">
                        <img
                            src={`${
                              item?.Item?.itemImages[0]?.image
                                ? `${NODE_API_URL}/static/itemImageFile/${item?.Item?.itemImages[0]?.image}`
                                : `${NODE_API_URL}/static/itemImageFile/${item?.itemImage}`
                            }`}
                            className="w-16 md:w-24 h-16 md:h-24 max-w-full max-h-full rounded mr-3"
                            // alt="Apple Watch"
                          />
                          {item?.Item?.itemName
                            ? item?.Item?.itemName
                            : item?.name}
                          </div>
                        </td>
                        <td className="px-6 py-4">
                          <div className="flex items-center justify-center">
                            <Button variant="link" size="icon" className="h-4 ">
                              <Minus
                                className="h-4 w-4 text-gray-500"
                                onClick={() => {
                                  !user
                                    ? cart.updateQuantity(index, "decrement")
                                    : handleMinusQty(
                                        item?.quantity,
                                        index,
                                        item
                                      );
                                }}
                              />
                            </Button>
                            <div className="font-bold">
                              <h4 id="counting">{item.quantity}</h4>
                            </div>
                            <Button variant="link" size="icon" className="h-4">
                              <Plus
                                className="h-4 w-4 text-gray-500"
                                onClick={() => {
                                  !user
                                    ? cart.updateQuantity(index, "increment")
                                    : handleAddQty(item?.quantity, index, item);
                                }}
                              />
                            </Button>
                          </div>
                        </td>
                        <td className="px-6 py-4 font-semibold text-gray-900 dark:text-white text-right">
                          AED {item.amount}
                        </td>
                        <td className="px-6 py-4 text-center">
                          {/* <Trash2
                              size={15}
                              onClick={() => showDeleteBox(item.id)}
                              id={item.id}
                            /> */}
                          <ConfirmBox
                            trigger={<Trash2 size={15} className="pointer" />}
                          >
                            <p className="text-center">Are you sure?</p>
                            <div className="text-center mt-2">
                              {user ? (
                                <Button
                                  size="xs"
                                  color="default"
                                  //   onClick={(id: number) => deletecartItem(item.id)}
                                  onClick={() => deletecartItem(item.id)}
                                >
                                  Yes
                                </Button>
                              ) : (
                                <Button
                                  size="xs"
                                  color="default"
                                  //   onClick={(id: number) => deletecartItem(item.id)}
                                  onClick={() => cart.remove(index)}
                                >
                                  Yes
                                </Button>
                              )}
                            </div>
                          </ConfirmBox>
                          {/* <div className="relative">
                            <Trash2
                              size={15}
                              onClick={() => showDeleteBox(item.id)}
                              id={item.id}
                            />

                            {deleteBox ? (
                              <div className="absolute w-84 bg-white shadow-lg rounded popup">
                                <div className="popup-body">
                                  <div className="absolute left-auto right-2">
                                    <Cross size={20} onClick={hideDeleteBox} />
                                  </div>
                                  <div className="text-center">
                                    Are you sure?
                                  </div>
                                  <div className="text-center mt-2">
                                    {user ? (
                                      <Button
                                        size="xs"
                                        color="default"
                                        //   onClick={(id: number) => deletecartItem(item.id)}
                                        onClick={() => deletecartItem(item.id)}
                                      >
                                        Yes
                                      </Button>
                                    ) : (
                                      <Button
                                        size="xs"
                                        color="default"
                                        //   onClick={(id: number) => deletecartItem(item.id)}
                                        onClick={() => cart.remove(index)}
                                      >
                                        Yes
                                      </Button>
                                    )}
                                  </div>
                                </div>
                              </div>
                            ) : null}
                          </div> */}
                        </td>
                      </tr>
                    </tbody>
                  );
                })}
              </>
            ) : (
              <>
                <tr>
                  <td></td>
                  <td></td>
                  {/* <td></td> */}
                  <td>
                    <p className="mt-10 text-center text-lg">
                      <strong>Cart Is Empty</strong>
                    </p>
                    <p className="mt-5 text-center text-lg">
                      <strong>
                        <Link href={"/Home"}>Go To Home</Link>
                      </strong>
                    </p>
                  </td>
                  <td></td>
                  <td></td>
                </tr>
              </>
            )}
          </table>

        </div>
          <div className="pb-5 mx-auto max-w-screen-lg">
            <div className="mt-0 border-t border-b py-2">
              <div className="flex flex-col justify-end">
                <div className="max-w-sm ms-auto">
                  <div className="flex items-center gap-0 px-5">
                    <p className="text-sm text-gray-400">Subtotal</p>
                    <div className="text-lg font-semibold text-gray-900 text-right w-32">
                    AED {subTotal}
                    </div>
                  </div>
                  <div className="flex items-center gap-5 px-5 text-right">
                    <div className="text-sm text-gray-400">Total</div>
                    <div className="text-lg font-semibold text-gray-900 text-right w-32">
                    AED {subTotal}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div className="text-end mx-auto max-w-screen-lg px-3">
          {cartList.length > 0 ? (
            <Button
              size="sm"
              color="default"
              //   onClick={(id: number) => deletecartItem(item.id)}
              onClick={checkoutCart}
            >
              CHECKOUT ( {subTotal} )
            </Button>
          ) : (
            <Button
              size="sm"
              color="default"
              //   onClick={(id: number) => deletecartItem(item.id)}
              onClick={checkoutCart}
              disabled
            >
              CHECKOUT ( {subTotal} )
            </Button>
          )}
        </div>
      </section>

      {/* <section className="h-auto bg-gray-100 py-12 sm:py-16 lg:py-10">
            <div className="mx-auto max-w-screen-lg bg-white shadow">
            <div className="flex lg:flex-row flex-col p-5">
                <div className="basis-1/2">
                  <p className="text-xl font-semibold text-gray-900 py-2 lg:visible invisible">
                    Products
                  </p>
                </div>
                <div className="lg:basis-1/4">
                <p className="text-xl font-semibold text-gray-900 py-2 lg:visible invisible text-center">
                    Quantity
                  </p>
                </div>
                <div className="lg:basis-1/4 items-center justify-center">
                <p className="text-xl font-semibold text-gray-900 py-2 lg:visible invisible text-center">Price</p>
                </div>
                <div className="lg:basis-1/4 items-center justify-center">
                <p className="text-xl font-semibold text-gray-900 py-2 lg:visible invisible text-center">Action</p>
                </div>
              </div>
              <div className="flex lg:flex-row flex-col- p-5">
                <div className="lg:basis-1/2">
                  <div className="shrink-0 flex flex-row items-center gap-2">
                    <img
                      className="h-20 w-20 sm:max-w-xs lg:max-w-screen-sm rounded-lg object-cover mr-2"
                      src="/dummy.jpg"
                      alt=""
                    />
                    <p className="text-base font-semibold text-gray-900">Cake Cake Cake Cake Cake Cake </p>
                  </div>
                </div>
                <div className="sm:basis-1/4 lg:basis-1/4">
                  <div className="flex lg:h-24 items-start lg:items-center lg:justify-center">
                    <div className="flex h-8 items-stretch text-gray-600">
                      <button className="flex items-center justify-center rounded-l-md bg-gray-200 px-4 transition hover:bg-black hover:text-white">
                        -
                      </button>
                      <div className="flex w-full items-center justify-center bg-gray-100 px-4 text-xs uppercase transition">
                        1
                      </div>
                      <button className="flex items-center justify-center rounded-r-md bg-gray-200 px-4 transition hover:bg-black hover:text-white">
                        +
                      </button>
                    </div>
                  </div>
                </div>
                <div className="lg:basis-1/4 items-center justify-center">
                <div className="flex items-start lg:h-24 lg:items-center justify-end">₹ 4794</div>
                </div>
                <div className="lg:basis-1/4 items-center justify-center">
                  <div className="flex items-start lg:h-24 lg:items-center justify-center">
                    <Link href={""}>
                      <Trash2 size={15} />
                    </Link>
                  </div>
                </div>
              </div>
    
              <div className="py-5">
                <div className="mt-6 border-t border-b py-2">
                  <div className="flex flex-col justify-end">
                    <div className="max-w-sm ms-auto">
                      <div className="flex items-center gap-5 px-5">
                        <p className="text-sm text-gray-400">Subtotal</p>
                        <div className="text-lg font-semibold text-gray-900 text-right w-32">
                          ₹ 47564.45
                        </div>
                      </div>
                      <div className="flex items-center gap-5 px-5 text-right">
                        <div className="text-sm text-gray-400">Shipping</div>
                        <div className="text-lg font-semibold text-gray-900 text-right w-32">
                          ₹ 94.95
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section> */}
    </>
  );
}

export default CartItemList;
