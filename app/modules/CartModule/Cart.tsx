import { addPlaceOrder, getCartList, getUserAddressList } from "@/app/api/apis";
import AuthContext from "@/app/contexts/auth/auth.context";
import CartCountContext from "@/app/contexts/cartCount/cartCount.context";
import { toastAlert } from "@/app/shared/components/Layout";
import { useCart } from "@/app/shared/hooks/use-cart/use-cart.hook";
import Checkbox from "@/components/ui/checkbox";
import { Button } from "@/components/ui/button";
import { Card, CardContent } from "@/components/ui/card";
import { Separator } from "@/components/ui/separator";
import { ADDRESS_TYPE } from "@/app/utils/constants";
import { addressObjToText, handleServerError } from "@/app/utils/helpers";
import { ShoppingBagIcon } from "@heroicons/react/24/solid";
import { Plus } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useMemo, useState } from "react";
import AddressForm from "./components/AddressForm";
import AddressList from "./components/AddressList";
import ItemList from "./components/ItemList";
import PaymentType from "./components/PaymentType";
import WithoutLoginAddressForm from "./components/WithoutLoginAddressForm";

function Cart() {
  const { authState }: any = useContext(AuthContext);
  const cart = useCart();

  const router = useRouter();
  const { cartCountDispatch }: any = useContext(CartCountContext);

  const [showAddress, setShowAddress] = useState<boolean>(false);
  const [isBillAsShip, setIsBillAsShip] = useState<boolean>(false);
  const [btnLoader, setBtnLoader] = useState<boolean>(false);
  const [addressList, setAddressList] = useState<any>([]);
  const [selectedBilling, setSelectedBilling] = useState<any>(null);
  const [selectedShipping, setSelectedShipping] = useState<any>(null);
  const [cartList, setCartList] = useState<any>([]);
  const [deliveryCharge, setDeliveryCharge] = useState<any>(0);
  const [totalAmount, setTotalAmount] = useState<any>(0);

  const [paymentType, setPaymentType] = useState("CashOnDelivery");

  const changePaymentType = (e: any) => {
    setPaymentType(e.target.value);
  };

  useEffect(() => {
    if (authState.authenticated) {
      getUserAddress();
      getCartDetails();
    } else {
      // router.push("/Login");
    }

    return () => {};
  }, []);

  // get cart list
  const getCartDetails = async () => {
    try {
      let cartlist = await getCartList();
      setCartList(cartlist.data.data);
    } catch (error: any) {
      handleServerError(error);
    }
  };

  const showAddressFrom = () => {
    setShowAddress(true);
  };

  // hide address form
  const hideAddressFrom = () => {
    setShowAddress(false);
  };

  // handle billing as shipping
  const handleBillAsShip = (event: any) => {
    if (event.target.checked) {
      setIsBillAsShip(true);
      let deliveryCharge = selectedBilling?.cityName == "Dubai" ? 10 : 20;
      setDeliveryCharge(deliveryCharge);

      let totalAmount = subTotal + deliveryCharge;
      setTotalAmount(totalAmount);

      setSelectedShipping({});
    } else {
      let deliveryCharge = selectedShipping?.cityName == "Dubai" ? 10 : 20;
      setDeliveryCharge(deliveryCharge);

      let totalAmount = subTotal + deliveryCharge;
      setTotalAmount(totalAmount);
      setIsBillAsShip(false);
    }
  };

  // get customer address list
  const getUserAddress = async () => {
    try {
      //@ts-ignore
      let userId = JSON.parse(localStorage.getItem("user_data")).id;

      let addressList = await getUserAddressList(userId);

      setAddressList(addressList.data.data);
    } catch (error: any) {
      handleServerError(error);
    }
  };

  // when change or select address
  const changeAddress = (address: any, type: string) => {
    if (type == ADDRESS_TYPE.BILLING) {
      setSelectedBilling(address);
      
      let deliveryCharge = address?.cityName == "Dubai" ? 10 : 20;
      
      setDeliveryCharge(deliveryCharge);

      let totalAmount = subTotal + deliveryCharge;
      setTotalAmount(totalAmount);
    } else if (type == ADDRESS_TYPE.SHIPPING) {
      setSelectedShipping(address);
      let deliveryCharge = address?.cityName == "Dubai" ? 10 : 20;
      setDeliveryCharge(deliveryCharge);

      let totalAmount = subTotal + deliveryCharge;
      setTotalAmount(totalAmount);
    }
  };

  const subTotal = useMemo(() => {
    let cartIndex = 0;
    let tempSubTotal = 0;
    if (authState.authenticated) {
      while (cartIndex < cartList.length) {
        tempSubTotal += cartList[cartIndex].amount;
        cartIndex++;
      }
    } else {
      while (cartIndex < cart.items.length) {
        tempSubTotal += cart.items[cartIndex].amount;
        cartIndex++;
      }
    }
    return tempSubTotal;
  }, [cartList, cart.items]);

  const placeOrder = async () => {
    // if (!authState.authenticated) {
    //   return router.push("/Login");
    // }

    let cartIndex = 0;
    let cartDetails = [];
    while (cartIndex < cartList.length) {
      cartDetails.push({
        ...cartList[cartIndex].Item,
        itemId: cartList[cartIndex].itemId,
        QTY: cartList[cartIndex].quantity,
        amount: cartList[cartIndex].amount,
      });

      cartIndex++;
    }

    // return;

    try {
      if (!cartList && !subTotal) {
        toastAlert("error", "Cart is empty");
        return;
      }

      if (isBillAsShip) {
        if (!selectedBilling) {
          toastAlert("error", "Please select billing address");
          return;
        }
      } else {
        if (!selectedBilling) {
          toastAlert("error", "Please select billing address");
          return;
        }
        if (!selectedShipping || Object.keys(selectedShipping).length == 0) {
          toastAlert("error", "Please select shipping address");
          return;
        }
      }
      // return

      let placeOrderObj = {};

      if (isBillAsShip) {
        placeOrderObj = {
          // billingAddressID: selectedBilling.id,
          billingAddress: selectedBilling
            ? addressObjToText(selectedBilling)
            : null,
          billingAsShipping: isBillAsShip,
          paymentType: paymentType,
          subTotal: subTotal,
          deliveryCharges: deliveryCharge,
          total: totalAmount ? totalAmount : subTotal,
          cartList: cartDetails,
          isInvoice: true,
        };
      } else {
        placeOrderObj = {
          // billingAddressID: selectedBilling.id,
          billingAddress: selectedBilling
            ? addressObjToText(selectedBilling)
            : null,
          // shippingAddressID: selectedShipping.id,
          shippingAddress: selectedShipping
            ? addressObjToText(selectedShipping)
            : null,
          billingAsShipping: isBillAsShip,
          paymentType: paymentType,
          subTotal: subTotal,
          deliveryCharges: deliveryCharge,
          total: totalAmount ? totalAmount : subTotal,
          cartList: cartDetails,
          isInvoice: true,
        };
      }

      // return
      setBtnLoader(true);
      let result = await addPlaceOrder(placeOrderObj);
      toastAlert("success", result.data.msg);
      cart.clear();
      cartCountDispatch({ type: "EMPTY_ITEMS" });
      setBtnLoader(false);
      router.push("/MyOrder");
    } catch (error: any) {
      setBtnLoader(false);
      handleServerError(error);
    }
  };

  const handlDeliveryCharge = (event: any) => {
    console.log(event, "event");
    if (event == "Dubai") {
      let deliveryCharge = 10;
      setDeliveryCharge(deliveryCharge);

      let totalAmount = subTotal + deliveryCharge;
      setTotalAmount(totalAmount);
    } else {
      let deliveryCharge = 20;
      setDeliveryCharge(deliveryCharge);

      let totalAmount = subTotal + deliveryCharge;
      setTotalAmount(totalAmount);
    }
  };

  return (
    <>
      <div className="w-full justify-center p-10 items-center">
        <Card className="w-full">
          <div className="divide-y divide-slate-200">
            <div className="p-6">
              <div className="flex justify-between">
                <div className="flex gap-2">
                  <div>
                    <Image
                      src="/33BH_01.png"
                      width={75}
                      height={500}
                      alt="Picture of the author"
                    />
                  </div>
                </div>
                <div>
                  <ShoppingBagIcon width={30} className="text-sky-600" />
                </div>
              </div>
            </div>
            <div className="">
              <CardContent className="p-0">
                <div className="grid lg:grid-cols-2">
                  {/* <ScrollArea className="h-96"> */}
                  <div>
                    <div className="p-4">
                      <div className="mt-10 px-4 pt-8 lg:mt-0">
                        {/* <p className="text-xl font-medium">Payment Details</p> */}
                        <p className="text-gray-600 text-center py-5">
                          Express checkout options
                        </p>
                        <div className="grid grid-cols-3 gap-2">
                          <div className="bg-[#5A30F4] h-12  inset-y-0 flex justify-center items-center rounded-md border">
                            <Image
                              src="/payment/ShopPay.png"
                              width={75}
                              height={75}
                              alt="Picture of the author"
                              className="h-12 text-white object-cover p-1"
                            />
                          </div>
                          <div className="bg-yellow-500  inset-y-0 flex justify-center items-center rounded-md border">
                            <Image
                              src="/payment/PayPal-Logo.wine.png"
                              width={75}
                              height={75}
                              alt="Picture of the author"
                              className="object-none h-7"
                            />
                          </div>
                          <div className="bg-gray-950  inset-y-0 flex justify-center items-center rounded-md border">
                            <Image
                              // src={Illustration}
                              src="/payment/apple-pay.png"
                              width={75}
                              height={75}
                              alt="Picture of the author"
                              className="object-cover h-7"
                            />
                          </div>
                        </div>
                        <div className="flex justify-center py-2"></div>

                        <div className="grid gap-2">
                          <div>
                            <div className="flex justify-between">
                              <div className="flex justify-between ">
                                {authState.authenticated ? (
                                  <>
                                    <div></div>

                                    {!showAddress ? (
                                      <div
                                        className="flex justify-between items-center cursor-pointer"
                                        onClick={showAddressFrom}
                                      >
                                        <p className="flex text-lg font-medium text-cyan-700">
                                          {/* <Link href="/Login"> */}{" "}
                                          <Plus size={25} />
                                          Add Address
                                          {/* </Link> */}
                                        </p>
                                      </div>
                                    ) : null}
                                  </>
                                ) : null}
                              </div>
                              {!authState.authenticated ? (
                                <div className="flex justify-between items-center">
                                  <p className="block text-lg  font-medium mr-2">
                                    {/* <Link href="/"> */}
                                    Have an account ?{/* </Link> */}
                                  </p>
                                  <p className="block text-sm font-medium text-cyan-700 underline">
                                    <Link href="/Login">Login</Link>
                                  </p>
                                </div>
                              ) : null}
                            </div>
                          </div>

                          {authState.authenticated ? null : (
                            <>
                              <p>
                                <strong>Add Address</strong>
                              </p>
                              <WithoutLoginAddressForm
                                setBtnLoader={setBtnLoader}
                                btnLoader={btnLoader}
                                handlDeliveryCharge={handlDeliveryCharge}
                                placeOrder={placeOrder}
                                changePaymentType={changePaymentType}
                                paymentType={paymentType}
                                subTotal={subTotal}
                                totalAmount={totalAmount}
                                cartItems={cart.items}
                                deliveryCharge={deliveryCharge}
                                setTotalAmount={setTotalAmount}
                                setDeliveryCharge={setDeliveryCharge}
                              />
                            </>
                          )}

                          {authState.authenticated ? (
                            <>
                              {showAddress ? (
                                <>
                                  <p>
                                    <strong>Add Address</strong>
                                  </p>
                                  <AddressForm
                                    hideAddressFrom={hideAddressFrom}
                                    setBtnLoader={setBtnLoader}
                                    btnLoader={btnLoader}
                                    getUserAddress={getUserAddress}
                                  />
                                </>
                              ) : (
                                <div className="row px-3">
                                  <div className="col col-12 col-md-6">
                                    <p className="mb-3">
                                      <strong>Delivery Location</strong>
                                    </p>
                                    {/* <p className="mb-3">Billing Address</p> */}
                                    {addressList.length > 0 ? (
                                      <Checkbox
                                        size="md"
                                        label="Billing as Shipping"
                                        id="123"
                                        onChange={(e: any) =>
                                          handleBillAsShip(e)
                                        }
                                      />
                                    ) : null}

                                    <p className="mb-3">
                                      <strong>Billing Address</strong>
                                    </p>
                                    {addressList.length > 0 ? (
                                      <AddressList
                                        addressList={addressList}
                                        changeAddress={changeAddress}
                                        type={ADDRESS_TYPE.BILLING}
                                        selectedAdderess={selectedBilling}
                                      />
                                    ) : (
                                      <p className="mb-3 text-center">
                                        Please Add Address
                                      </p>
                                    )}
                                  </div>

                                  {!isBillAsShip ? (
                                    <div className="col col-12 col-md-6">
                                      <p className="mb-3">
                                        <strong>Shipping Address</strong>
                                      </p>
                                      {addressList.length > 0 ? (
                                        <AddressList
                                          addressList={addressList}
                                          changeAddress={changeAddress}
                                          type={ADDRESS_TYPE.SHIPPING}
                                          selectedAdderess={selectedShipping}
                                        />
                                      ) : (
                                        <p className="mb-3 text-center">
                                          Please Add Address
                                        </p>
                                      )}
                                    </div>
                                  ) : null}
                                </div>
                              )}
                            </>
                          ) : null}

                          {authState.authenticated ? (
                            <>
                              <PaymentType
                                changePaymentType={changePaymentType}
                                paymentType={paymentType}
                              />

                              {paymentType == "CashOnDelivery" ||
                              paymentType == "OfflineBank" ? (
                                // <div>
                                <Button
                                  size={"lg"}
                                  className="bg-[#197AB9] h-14 text-lg"
                                  onClick={placeOrder}
                                >
                                  PLACE ORDER ( AED {subTotal} )
                                </Button>
                              ) : (
                                // </div>
                                // <div>
                                <Button
                                  size={"lg"}
                                  className="bg-[#197AB9] h-14 text-lg"
                                  onClick={placeOrder}
                                >
                                  PAY NOW ( AED {subTotal} )
                                </Button>
                                // </div>
                              )}
                            </>
                          ) : null}

                          {/* <p className="text-lg font-medium pt-8 pb-2">
                            Remember me
                          </p>
                          <div className="border p-5 rounded-md mb-5">
                            <div className="items-center flex space-x-2">
                              <Checkbox
                                label=""
                                id="terms1"
                                className="text-cyan-300"
                              />
                              <p className="text-sm text-muted-foreground">
                                Save my information for a faster checkout
                              </p>
                            </div>
                          </div> */}
                          {/* <Button
                            size={"lg"}
                            className="bg-[#197AB9] h-14 text-lg"
                          >
                            Pay now
                          </Button> */}
                        </div>
                      </div>
                    </div>
                    <Separator className="my-2" />
                    <div className="flex px-5 gap-2 items-center h-12">
                      <Link href="/" className="text-sm underline text-sky-600">
                        Return policy
                      </Link>
                      <Link href="/" className="text-sm underline text-sky-600">
                        Privacy policy
                      </Link>
                    </div>
                  </div>
                  <ItemList
                    cartList={authState.authenticated ? cartList : cart.items}
                    subTotal={subTotal}
                    deliveryCharge={deliveryCharge}
                    totalAmount={totalAmount}
                  />
                </div>
              </CardContent>
            </div>
          </div>
        </Card>
      </div>
    </>
  );
}

export default Cart;
