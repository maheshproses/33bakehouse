import React, { useEffect, useState } from "react";
import PageTitle from "../ProductsModule/components/PageTitle/PageTitle";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { dateFormater, handleServerError } from "@/app/utils/helpers";
import { toastAlert } from "@/app/shared/components/Layout";
import {
  SingleOrderByOrderNo,
  getOrderTrack,
  productOrderPDF,
} from "@/app/api/apis";
import Link from "next/link";
import { MoveRight, PrinterIcon } from "lucide-react";
import { NODE_API_URL } from "@/app/utils/constants";
import { useRouter } from "next/router";

function TrackOrder() {
  const router = useRouter();
  const OrderNo: any = router.query.OrderNo;

  const [emailOrderData, setEmailOrderData] = useState<any>([]);
  const [orderList, setOrderList] = useState<any>({});
  const [userData, setUserData] = useState<any>({});

  const [showCancelButton, setShowCancelButton] = useState(true);

  useEffect(() => {
    if (OrderNo) {
      OrderNoByData();
    }
    // Set a timeout to hide the cancel button after 30 seconds
    const timeoutId = setTimeout(() => {
      setShowCancelButton(false);
    }, 30000);

    // Clear the timeout if the component unmounts before 30 seconds
    return () => clearTimeout(timeoutId);
  }, []);

  const handleChangeInput = (e: any, label: string) => {
    setEmailOrderData({ ...emailOrderData, [label]: e.target.value });
  };

  const searchEmail = async () => {
    try {
      if (!emailOrderData) return;

      let order: any = await getOrderTrack(emailOrderData.search);
      console.log(order, "order");
      setUserData(
        order.data.data.UserAddress
          ? order.data.data.UserAddress
          : order.data.data
      );
      setOrderList(
        order.data.data.ItemOrders
          ? order.data.data?.ItemOrders[0]
          : order.data.data
      );
    } catch (error: any) {
      console.log(error, "errorrrrr");

      handleServerError(error);
      toastAlert("error", error?.data);
    }
  };

  const OrderNoByData = async () => {
    console.log(OrderNo, "OrderNo");

    let orderData: any = await SingleOrderByOrderNo(OrderNo);
    console.log(orderData, "orderData");
    setUserData(orderData.data.data.UserAddress);
    setOrderList(orderData.data.data);
  };

  const onClickPDF = async (id: number) => {
    let pdfObj = await productOrderPDF(id);
    console.log(pdfObj, "pdfObj");
    let url = `${NODE_API_URL}/static/ProductInvoicepdf/${pdfObj?.data?.data}`;

    var mapForm: any = document.createElement("form");
    mapForm.target = "_blank";
    mapForm.method = "GET"; // or "post" if appropriate
    mapForm.action = url;

    document.body.appendChild(mapForm);
    mapForm.submit();
  };

  return (
    <>
      <PageTitle pageTitle="Track Order" />
      {OrderNo ? null : (
        <div className="mx-auto max-w-2xl py-8">
          <div className="grid gap-5 grid-cols-2 items-center w-full">
            <div>
              <Input
                label="Enter your Email Address / Your Order No."
                type="email"
                isRequired
                placeholder="Enter your Email Address / Your Order No."
                onChange={(e: any) => {
                  handleChangeInput(e, "search");
                }}
                name="email"
                value={emailOrderData?.email}
              />
            </div>
            <div className="mt-2">
              <Button type="submit" onClick={searchEmail}>
                Search
              </Button>
            </div>
          </div>
        </div>
      )}
      <div className="mx-auto  py-1">
        <section className="bg-gray-100">
          <div className="max-w-screen-lg mx-auto py-5 ">
            <div className="bg-white shadow rounded-lg">
              <div className="grid grid-cols-1 lg:grid-cols-5 gap-5 px-5 py-5">
                <div className="grid grid-cols-2 lg:grid-cols-1">
                  <div>
                    <p className="font-semibold text-sm">Order Number:</p>
                    <p className="font-normal text-sm">
                      {orderList?.orderNo ? orderList?.orderNo : "-"}
                    </p>
                  </div>
                  <div>
                    <p className="font-semibold text-sm">Order Placed:</p>
                    <p className="font-normal text-sm">
                      {orderList?.createdAt
                        ? dateFormater(orderList?.createdAt)
                        : "-"}
                      {/* 11-4-2222 */}
                    </p>
                  </div>
                  <div>
                    <p className="font-semibold text-sm">Mobile No.:</p>
                    <p className="font-normal text-sm">
                      {userData?.mobile ? userData?.mobile : "-"}
                    </p>
                  </div>
                </div>
                <div className="grid grid-cols-1 col-span-2">
                  <div>
                    <p className="font-semibold text-sm">Shipping Address:</p>
                    <p className="font-normal text-sm break-all">
                      {orderList?.shippingAddress
                        ? orderList?.shippingAddress
                        : "-"}
                    </p>
                  </div>
                </div>
                <div className="grid grid-cols-2 col-span-2">
                  <div className="grid grid-cols-1">
                    <div className="py-2">
                      <p className="font-semibold text-sm font-primary">
                        Order Status:
                      </p>
                      <span className="inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">
                        {orderList?.orderStatus ? orderList?.orderStatus : "-"}
                      </span>
                    </div>
                    <div>
                      <p className="font-semibold text-sm">Total Amount:</p>
                      <p className="font-semibold text-sm">
                        AED {orderList?.total ? orderList?.total : "-"}
                      </p>
                    </div>
                  </div>
                  <div className="grid grid-cols-1 place-items-end">
                    <div className="flex items-start gap-5 py-2">
                      <div>
                        {/* <Link href={""}> */}
                        <PrinterIcon
                          className="h-5 w-5"
                          onClick={() => onClickPDF(orderList.id)}
                        />
                        {/* </Link> */}
                      </div>
                      {/* <div onClick={() => toggleProductList(index)}>
                        {showProducts == index ? (
                          <DownArrow className="mx-2 pointer" />
                        ) : (
                          <RightArrow className="mx-2 pointer" />
                        )}
                      </div> */}
                    </div>
                    {showCancelButton && (
                      <div>
                        <Button variant="destructive">Cancel</Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>

              {/* {showProducts == index ? (
                <> */}
              <div className="px-4 py-5 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-10 lg:px-8 lg:py-5">
                <p className="font-semibold text-sm">Order Status:</p>
                <div className="grid gap-5 lg:grid-cols-6 sm:grid-cols-2">
                  {orderList?.OrderStatusHistories?.map((status: any) => {
                    return (
                      <div
                        className="flex items-center flex-col xl:flex-row text-center"
                        key={status?.id}
                      >
                        <div className="rounded-full  w-4 h-4 bg-green-400"></div>
                        <div className="py-2 px-2">
                          <div className="flex flex-col">
                            <p className="font-medium text-sm">
                              {status.orderStatus ? status.orderStatus : "-"}
                            </p>
                            <p className="font-medium text-sm">
                              {status.createdAt
                                ? dateFormater(status.createdAt)
                                : "-"}
                            </p>
                          </div>
                        </div>
                        <div>
                          <MoveRight className="transform rotate-90 sm:rotate-0 text-green-400" />
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>

              {orderList?.ItemOrderDetails?.map(
                (product: any, index: number) => {
                  return (
                    <>
                      <div
                        className="grid grid-cols-2 gap-5 px-5 items-center border-collapse border-y bg-gray-100/40 p-2"
                        key={product?.id}
                      >
                        <div className="flex items-center gap-5">
                          <img
                            className="lg:h-24 lg:w-24 w-24 max-w-md h-auto sm:max-w-xs lg:max-w-screen-lg rounded object-cover"
                            src={`${
                              product.Item?.itemImages[0]?.image
                                ? `${NODE_API_URL}/static/itemImageFile/${product?.Item?.itemImages[0]?.image}`
                                : "no_image.png"
                            }`}
                            alt=""
                          />
                          <div>
                            <p className="text-xl font-semibold text-gray-900">
                              {product?.Item?.itemName}
                            </p>
                            <p className="text-sm font-medium text-gray-900">
                              Qty {product.quantity}
                            </p>
                            <p className="text-sm font-medium text-gray-900">
                              Unit Price: AED {product?.Item?.basicPrice}
                            </p>
                          </div>
                        </div>
                        <div className="text-right text-base font-semibold text-gray-900">
                          AED {product.amount}
                        </div>
                      </div>
                    </>
                  );
                }
              )}

              <div className="border-b-2 border-collapse border-slate-200/50"></div>
              <div className="flex items-end justify-end gap-5 mb-5 py-5">
                <div className="grid grid-rows-4">
                  <div className="grid grid-cols-2 text-right px-5">
                    <div>SubTotal</div>
                    <div>AED {orderList.subTotal}</div>
                  </div>
                  <div className="grid grid-cols-2 text-right px-5">
                    <div>Delivery Charge</div>
                    <div>AED {orderList.deliveryCharges}</div>
                  </div>
                  <div className="grid grid-cols-2 text-right px-5">
                    <div>
                      <p className="text-xl font-semibold text-gray-900">
                        Total
                      </p>
                    </div>
                    <div>
                      <p className="text-lg font-semibold text-gray-900">
                        AED {orderList.total}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              {/* </>
              ) : null} */}
            </div>
          </div>
        </section>
      </div>
    </>
  );
}

export default TrackOrder;
