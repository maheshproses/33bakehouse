import DownArrow from "@/app/icons/DownArrow";
import RightArrow from "@/app/icons/RightArrow";
import { NODE_API_URL } from "@/app/utils/constants";
import { dateFormater } from "@/app/utils/helpers";
import { Button } from "@/components/ui/button";
import { MoveRight, PrinterIcon } from "lucide-react";
import Link from "next/link";

function SingleOrder({
  item,
  index,
  showProducts,
  cancleOrder,
  toggleProductList,
  onClickPDF,
  btnLoader,
  pdfLoader,
  showCancelButton,
}: any) {
  console.log(item, "item");

  return (
    <section className="bg-gray-100">
      <div className="max-w-screen-lg mx-auto py-5 ">
        <div className="bg-white shadow rounded-lg">
          <div className="grid grid-cols-1 lg:grid-cols-5 gap-5 px-5 py-5">
            <div className="grid grid-cols-2 lg:grid-cols-1">
              <div>
                <p className="font-semibold text-sm">Order Number:</p>
                <p className="font-normal text-sm">{item.orderNo}</p>
              </div>
              <div>
                <p className="font-semibold text-sm">Order Placed:</p>
                <p className="font-normal text-sm">
                  {dateFormater(item.createdAt)}
                </p>
              </div>
              <div>
                <p className="font-semibold text-sm">Mobile No.:</p>
                <p className="font-normal text-sm">{item?.User?.mobile}</p>
              </div>
            </div>
            <div className="grid grid-cols-1 col-span-2">
              <div>
                <p className="font-semibold text-sm">Billing Address:</p>
                <p className="font-normal text-sm break-all">
                  {item.billingAddress}
                </p>
              </div>
              {item.billingAsShipping == false ? (
                <div>
                  <p className="font-semibold text-sm">Shipping Address:</p>
                  <p className="font-normal text-sm break-all">
                    {item.shippingAddress}
                  </p>
                </div>
              ) : null}
            </div>
            <div className="grid grid-cols-2 col-span-2">
              <div className="grid grid-cols-1">
                <div className="py-2">
                  <p className="font-semibold text-sm font-primary">
                    Order Status:
                  </p>
                  <span className="inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">
                    {item.orderStatus}
                  </span>
                </div>
                <div>
                  <p className="font-semibold text-sm">Total Amount:</p>
                  <p className="font-semibold text-sm">AED {item.total}</p>
                </div>
              </div>
              <div className="grid grid-cols-1 place-items-end">
                <div className="flex items-start gap-5 py-2">
                  <div>
                    {/* <Link href={""}> */}
                    <PrinterIcon
                      className="h-5 w-5"
                      onClick={() => onClickPDF(item.id, index)}
                    />
                    {/* </Link> */}
                  </div>
                  <div onClick={() => toggleProductList(index)}>
                    {showProducts == index ? (
                      // <ChevronRight className="h-5 w-5" />
                      <DownArrow className="mx-2 pointer" />
                    ) : (
                      <RightArrow className="mx-2 pointer" />
                    )}
                  </div>
                </div>
                {showCancelButton && (
                  <div>
                    <Button variant="destructive">Cancel</Button>
                  </div>
                )}
              </div>
            </div>
          </div>
          {/* <div className="grid grid-cols-2 items-center border-collapse border-y bg-gray-100/40 gap-5 px-5 py-5">
        <div className="flex items-center gap-5">
            <div>
              <img
                className="lg:h-24 lg:w-24 w-24 h-auto sm:max-w-xs lg:max-w-screen-lg rounded object-cover"
                src="/dummy.jpg"
                alt=""
              />
            </div>
            <div>
              <p className="text-xl font-semibold text-gray-900">Cookies</p>
              <p className="text-sm font-medium text-gray-900">Qty 3</p>
            </div>
          </div>
          <div className="text-right text-base font-semibold text-gray-900">
            AED 799.5
          </div>
        </div> */}

          {showProducts == index ? (
            <>
              <div className="px-4 py-5 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-10 lg:px-8 lg:py-5">
                <p className="font-semibold text-sm">Order Status:</p>
                <div className="grid gap-5 lg:grid-cols-6 sm:grid-cols-2">
                  {item.OrderStatusHistories.map((status: any) => {
                    return (
                      <div
                        className="flex items-center flex-col xl:flex-row text-center"
                        key={status?.id}
                      >
                        <div className="rounded-full  w-4 h-4 bg-green-400"></div>
                        <div className="py-2 px-2">
                          <div className="flex flex-col">
                            <p className="font-medium text-sm">
                              {status.orderStatus}
                            </p>
                            <p className="font-medium text-sm">
                              {dateFormater(status.createdAt)}
                            </p>
                          </div>
                        </div>
                        {status.orderStatus == "Delivered" ? null : (
                          <div>
                            <MoveRight className="transform rotate-90 sm:rotate-0 text-green-400" />
                          </div>
                        )}
                      </div>
                    );
                  })}
                  {/* <div className="flex items-center flex-col xl:flex-row text-center">
                    <div className="rounded-full  w-4 h-4 bg-yellow-400"></div>
                    <div className="py-2 px-2">
                      <div className="flex flex-col">
                        <p className="font-medium text-sm">Processing</p>
                        <p className="font-medium text-sm">20/11/2023</p>
                      </div>
                    </div>
                    <div>
                      <MoveRight className="transform rotate-90 sm:rotate-0 text-yellow-400" />
                    </div>
                  </div> */}
                </div>
              </div>

              {item?.ItemOrderDetails.map((product: any, index: number) => {
                return (
                  <>
                    <div
                      className="grid grid-cols-2 gap-5 px-5 items-center border-collapse border-y bg-gray-100/40 p-2"
                      key={product?.id}
                    >
                      <div className="flex items-center gap-5">
                        <div className="relative ">
                          <img
                            className="lg:h-24 lg:w-24 w-24 max-w-md h-auto sm:max-w-xs lg:max-w-screen-lg rounded object-cover"
                            src={`${
                              product.Item?.itemImages[0]?.image
                                ? `${NODE_API_URL}/static/itemImageFile/${product?.Item?.itemImages[0]?.image}`
                                : "no_image.png"
                            }`}
                            alt=""
                          />
                          <div className="absolute inline-flex items-center  justify-center w-5 h-5 text-xs font-medium text-white bg-gray-500 b rounded-full -top-2 -right-3 dark:border-gray-900">
                            {product.quantity}
                          </div>
                        </div>
                        <div>
                          <p className="text-xl font-semibold text-gray-900">
                            {product?.Item.itemName}
                          </p>
                          <p className="text-sm font-medium text-gray-900">
                            Qty: {product.quantity}
                          </p>
                          <p className="text-sm font-medium text-gray-900">
                            Unit Price: AED {product?.Item?.basicPrice}
                          </p>
                        </div>
                      </div>
                      <div className="text-right text-base font-semibold text-gray-900">
                        AED {product.amount}
                      </div>
                    </div>
                  </>
                );
              })}

              <div className="border-b-2 border-collapse border-slate-200/50"></div>
              <div className="flex items-end justify-end gap-5 mb-5 py-5">
                <div className="grid grid-rows-4">
                  <div className="grid grid-cols-2 text-right px-5">
                    <div>SubTotal</div>
                    <div>AED {item.subTotal}</div>
                  </div>
                  <div className="grid grid-cols-2 text-right px-5">
                    <div>Delivery Charge</div>
                    <div>
                      AED {item.deliveryCharges ? item.deliveryCharges : 0}
                    </div>
                  </div>
                  <div className="grid grid-cols-2 text-right px-5">
                    <div>
                      <p className="text-xl font-semibold text-gray-900">
                        Total
                      </p>
                    </div>
                    <div>
                      <p className="text-lg font-semibold text-gray-900">
                        AED {item.total}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : null}
        </div>
      </div>
    </section>
  );
}

export default SingleOrder;
