import React from "react";
import SingleOrder from "./SingleOrder";

function OrderList({
  allordersList,
  showProducts,
  toggleProductList,
  cancleOrder,
  onClickPDF,
  btnLoader,
  pdfLoader,
  showCancelButton
}: any) {
  return (
    <>
      {allordersList.map((item: any, index: number) => {
        return (
          <>
            <SingleOrder
              item={item}
              index={index}
              showProducts={showProducts}
              cancleOrder={cancleOrder}
              toggleProductList={toggleProductList}
              onClickPDF={onClickPDF}
              btnLoader={btnLoader}
              pdfLoader={pdfLoader}
              showCancelButton={showCancelButton}
            />
          </>
        );
      })}
    </>
  );
}

export default OrderList;
