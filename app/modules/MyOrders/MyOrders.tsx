import type { NextPage } from "next";
import {
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
} from "@/components/ui/collapsible";
import {
  ArrowDown,
  ArrowLeftIcon,
  ChevronDownIcon,
  ChevronRight,
  HomeIcon,
  MinusIcon,
  MoveRight,
  Printer,
  PrinterIcon,
} from "lucide-react";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import PageTitle from "../ProductsModule/components/PageTitle/PageTitle";
import { useEffect, useState } from "react";
import { getOrders, productOrderPDF } from "@/app/api/apis";
import OrderList from "./components/OrderList";
import { toastAlert } from "@/app/shared/components/Layout";
import { NODE_API_URL } from "@/app/utils/constants";

function MyOrders() {
  const [allordersList, setAllOrdersList] = useState<any>([]);
  const [showProducts, setshowProducts] = useState<any>(null);
  const [btnLoader, setBtnLoader] = useState<number | null>(null);
  const [pdfLoader, setPDFLoader] = useState<number | null>(null);

  const [showCancelButton, setShowCancelButton] = useState(true);

  useEffect(() => {

    getOrderDetails();
    // Set a timeout to hide the cancel button after 30 seconds
    const timeoutId = setTimeout(() => {
      setShowCancelButton(false);
    }, 30000);

    // Clear the timeout if the component unmounts before 30 seconds
    return () => clearTimeout(timeoutId);
  }, []);


  // get orders list
  const getOrderDetails = async () => {
    try {
      let orderlist = await getOrders();
      console.log(orderlist.data.data, "orderlisttt");
      setAllOrdersList(orderlist.data.data);
      // let index = 0;
      // let abc = orderlist.data.data
      // while (index < abc.length) {
      //   console.log(abc[index], "abc");
      //   let currentStatus = abc[index].orderStatus
      //   let orderID = abc[index].id
      //   let currentData = abc[index].createdAt
      //   console.log(abc[index].OrderStatusHistories, "OrderStatusHistories");
      //   abc[index].OrderStatusHistories.push({

      //     orderID: orderID,
      //     orderStatus: currentStatus,
      //     createdAt: currentData

      //   })

      //   index++
      // }
    } catch (error: any) {
      console.log(error, "error");
    }
  };

  const toggleProductList = (index: number) => {
    console.log(index,"indexxxxx");
    
    if (index == showProducts) {
      setshowProducts(null);
    } else {
      setshowProducts(index);
    }
  };

  const onClickPDF = async (id: number, index: number) => {
    try {
console.log(id,"id");

      setPDFLoader(index);
      let pdfObj = await productOrderPDF(id);
      console.log(pdfObj?.data?.data,"pdfObj?.data?.data");
      
      let url = `${NODE_API_URL}/static/ProductInvoicepdf/${pdfObj?.data?.data}`;

      var mapForm: any = document.createElement("form");
      mapForm.target = "_blank";
      mapForm.method = "GET"; // or "post" if appropriate
      mapForm.action = url;

      document.body.appendChild(mapForm);
      mapForm.submit();
      setPDFLoader(null);

    } catch (error: any) {
      setPDFLoader(null);
      console.log(error, "error");
      toastAlert("error", error);
    }
  }

  return (
    <>
      <PageTitle pageTitle="My Order" />

      <OrderList
        allordersList={allordersList}
        showProducts={showProducts}
        // cancleOrder={cancleOrder}
        toggleProductList={toggleProductList}
        onClickPDF={onClickPDF}
        btnLoader={btnLoader}
        pdfLoader={pdfLoader}
        showCancelButton = {showCancelButton}
      />

      {/* <section className="bg-gray-100">
        <div className="max-w-screen-lg mx-auto py-5 ">
          <div className="bg-white shadow rounded-lg">
            <div className="grid grid-cols-1 lg:grid-cols-5 gap-5 px-5 py-5">
              <div className="grid grid-cols-2 lg:grid-cols-1">
                <div>
                  <p className="font-semibold text-sm">Order Number:</p>
                  <p className="font-normal text-sm">146</p>
                </div>
                <div>
                  <p className="font-semibold text-sm">Order Placed:</p>
                  <p className="font-normal text-sm">20/11/2023</p>
                </div>
              </div>
              <div className="grid grid-cols-1 col-span-2">
                <div>
                  <p className="font-semibold text-sm">Billing Address:</p>
                  <p className="font-normal text-sm">
                    125, Ring Rd, near billabong school, Vadsar, Kalali,
                    Vadodara, Gujarat 390012 , Vadodara, GUJRAT, India - 390012
                    Phone number 7069734873
                  </p>
                </div>
              </div>
              <div className="grid grid-cols-2 col-span-2">
                <div className="grid grid-cols-1">
                  <div className="py-2">
                    <p className="font-semibold text-sm font-primary">
                      Order Status:
                    </p>
                    <span className="inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">
                      Pending
                    </span>
                  </div>
                  <div>
                    <p className="font-semibold text-sm">Total Amount:</p>
                    <p className="font-semibold text-sm">AED 5799</p>
                  </div>
                </div>
                <div className="grid grid-cols-1 place-items-end">
                  <div className="flex items-start gap-5 py-2">
                    <div>
                      <Link href={""}>
                        <PrinterIcon className="h-5 w-5" />
                      </Link>
                    </div>
                    <div>
                      <Link href={""}>
                        <ChevronRight className="h-5 w-5" />
                      </Link>
                    </div>
                  </div>
                  <div>
                    <Button variant="destructive">Cancel</Button>
                  </div>
                </div>
              </div>
            </div>

            <div className="px-4 py-5 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-10 lg:px-8 lg:py-5">
              <p className="font-semibold text-sm">Order Status:</p>
              <div className="grid gap-5 lg:grid-cols-6 sm:grid-cols-2">
                <div className="flex items-center flex-col xl:flex-row text-center">
                  <div className="rounded-full  w-4 h-4 bg-green-400"></div>
                  <div className="py-2 px-2">
                    <div className="flex flex-col">
                      <p className="font-medium text-sm">Pending</p>
                      <p className="font-medium text-sm">20/11/2023</p>
                    </div>
                  </div>
                  <div>
                    <MoveRight className="transform rotate-90 sm:rotate-0 text-green-400" />
                  </div>
                </div>
                <div className="flex items-center flex-col xl:flex-row text-center">
                  <div className="rounded-full  w-4 h-4 bg-yellow-400"></div>
                  <div className="py-2 px-2">
                    <div className="flex flex-col">
                      <p className="font-medium text-sm">Processing</p>
                      <p className="font-medium text-sm">20/11/2023</p>
                    </div>
                  </div>
                  <div>
                    <MoveRight className="transform rotate-90 sm:rotate-0 text-yellow-400" />
                  </div>
                </div>
                <div className="flex items-center flex-col xl:flex-row text-center">
                  <div className="rounded-full  w-4 h-4 bg-green-600"></div>
                  <div className="py-2 px-2">
                    <div className="flex flex-col">
                      <p className="font-medium text-sm">Dispatch</p>
                      <p className="font-medium text-sm">20/11/2023</p>
                    </div>
                  </div>
                  <div>
                    <MoveRight className="transform rotate-90 sm:rotate-0 text-green-600" />
                  </div>
                </div>
                <div className="flex items-center flex-col xl:flex-row text-center">
                  <div className="rounded-full  w-4 h-4 bg-gray-500"></div>
                  <div className="py-2 px-2">
                    <div className="flex flex-col">
                      <p className="font-medium text-sm">Cancel</p>
                      <p className="font-medium text-sm">20/11/2023</p>
                    </div>
                  </div>
                  <div>
                    <MoveRight className="transform rotate-90 sm:rotate-0 text-gray-500" />
                  </div>
                </div>
                <div className="flex items-center flex-col xl:flex-row text-center">
                  <div className="rounded-full  w-4 h-4 bg-purple-400"></div>
                  <div className="py-2 px-2">
                    <div className="flex flex-col">
                      <p className="font-medium text-sm">Delivered</p>
                      <p className="font-medium text-sm">20/11/2023</p>
                    </div>
                  </div>
                  <div>
                  </div>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-5 px-5 items-center border-collapse border-y bg-gray-100/40 p-2">
              <div className="flex items-center gap-5">
                <img
                  className="lg:h-24 lg:w-24 w-24 max-w-md h-auto sm:max-w-xs lg:max-w-screen-lg rounded object-cover"
                  src="/dummy.jpg"
                  alt=""
                />
                <div>
                  <p className="text-xl font-semibold text-gray-900">Cake</p>
                  <p className="text-sm font-medium text-gray-900">Qty 1</p>
                </div>
              </div>
              <div className="text-right text-base font-semibold text-gray-900">
                AED 799.5
              </div>
            </div>
            <div className="grid grid-cols-2 gap-5 px-5 items-center mb-5 bg-gray-100/40 p-2">
              <div className="flex items-center gap-5">
                <div>
                  <img
                    className="lg:h-24 lg:w-24 w-24 max-w-md h-auto sm:max-w-xs lg:max-w-screen-lg rounded object-cover"
                    src="/dummy.jpg"
                    alt=""
                  />
                </div>
                <div>
                  <p className="text-xl font-semibold text-gray-900">Cake</p>
                  <p className="text-sm font-medium text-gray-900">Qty 1</p>
                </div>
              </div>
              <div className="text-right text-base font-semibold text-gray-900">
                AED 799.5
              </div>
            </div>
            <div className="border-b-2 border-collapse border-slate-200/50"></div>
            <div className="flex items-end justify-end gap-5 mb-5 py-5">
              <div className="grid grid-rows-4">
                <div className="grid grid-cols-2 text-right px-5">
                  <div>SubTotal</div>
                  <div>AED 5799</div>
                </div>
                <div className="grid grid-cols-2 text-right px-5">
                  <div>Shipping Cost</div>
                  <div>AED 5799</div>
                </div>
                <div className="grid grid-cols-2 text-right px-5">
                  <div>Tax</div>
                  <div>AED 5799</div>
                </div>
                <div className="grid grid-cols-2 text-right px-5">
                  <div>
                    <p className="text-xl font-semibold text-gray-900">Total</p>
                  </div>
                  <div>
                    <p className="text-lg font-semibold text-gray-900">
                      AED 5795
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> */}
    </>
  );
}

export default MyOrders;
