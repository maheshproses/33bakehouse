import { NODE_API_URL } from "@/app/utils/constants";
import type { NextPage } from "next";
import Link from "next/link";

const ProductList: NextPage = ({ productList, activeCategory }: any) => {

  
  return (
    <>
      <div>
        <div>
          <div className="flex items-center justify-between">
            <div className="pt-5 px-5">
              <h2 className="text-2xl font-semibold tracking-tight">
                {/* Product Category{" "} */}
                {activeCategory} Product
              </h2>
            </div>
          </div>
          <div className="shrink-0 bg-border h-[1px] w-full my-4"></div>
          <section
            id="Projects"
            className="w-fit mx-auto grid grid-cols-1 lg:grid-cols-3 md:grid-cols-2 justify-items-center justify-center gap-y-20 gap-x-14 mt-10 mb-5"
          >

            {productList.map((item: any) => (
              <div
                key={item.id}
                className="w-72 bg-white shadow-md rounded-xl duration-500 hover:scale-105 hover:shadow-xl"
              >
                <div>
                  <Link href={`/ProductDetail/?id=${item.id}`} className="link">
                    <div className="item-header">
                      <img
                        src={
                          item.itemImages[0]?.image
                            ? `${NODE_API_URL}/static/itemImageFile/${item.itemImages[0]?.image}`
                            : "no_image.png"
                        }
                        alt="product"
                        className="img-fluid h-80 w-72 object-cover rounded-t-xl"
                      />
                    </div>
                    <div className="px-4 py-3 w-72">
                      <p className="text-lg font-bold text-black truncate block capitalize">
                        {item.itemName}
                      </p>
                      <div className="flex items-center">
                        <p className="text-lg font-semibold text-black cursor-auto my-3">
                          AED {item.basicPrice}
                        </p>
                        <div className="ml-auto">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            fill="currentColor"
                            className="bi bi-bag-plus"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="M8 7.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0v-1.5H6a.5.5 0 0 1 0-1h1.5V8a.5.5 0 0 1 .5-.5z"
                            />
                            <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
                          </svg>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            ))}
          </section>
        </div>
      </div>
    </>
  );
};

export default ProductList;
