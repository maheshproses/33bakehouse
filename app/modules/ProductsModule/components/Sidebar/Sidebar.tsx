import { getAllCategory } from "@/app/api/apis";
import { Button } from "@/components/ui/button";
import { ScrollArea } from "@/components/ui/scroll-area";
import type { NextPage } from "next";
import Link from "next/link";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { useParams } from "next/navigation";

const Sidebar: NextPage = (props) => {
  const [categoryList, setCategoryList] = useState<any>([]);
  const [active, setActive] = useState<null | number>(
    Number(window.location.search.split("=")[1])
  );

  const params = useParams();

  useEffect(() => {
    const id = Number(window.location.search.split("=")[1]);
    getCategoryList();
    setActive(id);
    return () => {};
  }, []);

  const getCategoryList = async () => {
    try {
      let categoryList: any = await getAllCategory();
      setCategoryList(categoryList.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {/* <div className="relative w-64 bg-zinc-100 sidebar p-8">
  <div className="absolute left-0 top-300 w-64 z-30 h-full px-3 overflow-y-auto">
  <h1>CATEGORIES</h1>
            <h2 className="text-sm">Filter by Brands</h2>
            <h1>FILTER</h1>
            <h2 className="text-sm">Filter by Brands</h2>
  </div>
</div> */}

      {/* <div className="fixed left-0 top-50 w-64 bg-zinc-100 h-[calc(100%-250px)] sidebar p-8">
    <div className="h-full px-3 overflow-y-auto dark:bg-gray-400">
            <h1>CATEGORIES</h1>
            <h2 className="text-sm">Filter by Brands</h2>
        </div>
        <div className="h-full px-3 overflow-y-auto dark:bg-gray-400">
            <h1>FILTER</h1>
            <h2 className="text-sm">Filter by Brands</h2>
        </div>
    </div> */}

      <div className="pb-12">
        <div className="space-y-4 py-4">
          <div className="py-2 ">
            <h2 className="px-7 text-lg text-center font-semibold tracking-tight text-bhsecondary  ">
              Products Categories
            </h2>
            <div className="flex items-center justify-center sm:max-w-[100%]">
              <div className="lg:hidden md:hidden max-w-lg">
                <Select>
                  <SelectTrigger className="w-[180px]">
                    <SelectValue placeholder="Select a fruit" />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectGroup>
                      <SelectLabel>Fruits</SelectLabel>
                      <SelectItem value="apple">Apple</SelectItem>
                      <SelectItem value="banana">Banana</SelectItem>
                      <SelectItem value="blueberry">Blueberry</SelectItem>
                      <SelectItem value="grapes">Grapes</SelectItem>
                      <SelectItem value="pineapple">Pineapple</SelectItem>
                    </SelectGroup>
                  </SelectContent>
                </Select>
              </div>
            </div>
            <div className="shrink-0 bg-border h-[1px] w-full my-4"></div>
            <ScrollArea className="md:h-[400px] px-1 l">
              <div className="space-y-1 p-2">
                {/* {filteredItems?.map((item, i) => (
                <Button
                  key={item.id}
                  variant="ghost"
                  className="w-full justify-between font-bold"
                >
                  <p>{item.description}</p>
                  <p>{item.price}</p>
                </Button>
              ))} */}
                {categoryList?.map((item: any, index: number) => (
                  <Link
                    onClick={() => setActive(item.id)}
                    href={`/Products/?categoryId=${item.id}`}
                    key={item?.id}
                  >
                    <Button
                      key={item.id}
                      variant="ghost"
                      className={`w-full justify-between font-bold hover:bg-bhprimary/10 ${
                        active == item.id &&
                        "bg-bhprimary focus:outline-none ring bg-bhprimary/25"
                      }`}
                    >
                      {/* {playlist} */}
                      {item.title}
                      {/* <p>{item.price}</p> */}
                    </Button>
                  </Link>
                ))}
              </div>
            </ScrollArea>
          </div>
        </div>
      </div>
    </>
  );
};

export default Sidebar;
