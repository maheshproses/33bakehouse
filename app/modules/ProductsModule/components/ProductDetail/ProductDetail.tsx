import { addCart, getCartList, getProductByID } from "@/app/api/apis";
import CartCountContext from "@/app/contexts/cartCount/cartCount.context";
import { toastAlert } from "@/app/shared/components/Layout";
import { useCart } from "@/app/shared/hooks/use-cart/use-cart.hook";
import { Button } from "@/components/ui/button";
import { Card, CardContent } from "@/components/ui/card";
import { NODE_API_URL } from "@/app/utils/constants";
import { handleServerError } from "@/app/utils/helpers";
import { Minus, Plus } from "lucide-react";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import ImagePreview from "./ImagePreview";


function ProductDetail() {
  const router = useRouter();
  const { id } = router.query;

  const { cartCountDispatch }: any = useContext(CartCountContext);

  const [productData, setProductData] = useState<any>({});
  const [productImages, setProductImages] = useState<any[]>([]);
  const [productVideo, setProductVideo] = useState<any[]>([]);
  const [cartQTY, setCartQTY] = useState<number>(1);
  const [btnLoader, setBtnLoader] = useState<boolean>(false);
  const cart = useCart();
  const [show, setShow] = useState(false);
  const [amount, setAmount] = useState<any>();
  const [price, setPrice] = useState<any>();

  useEffect(() => {
    if (id) {
      getProductDataById();
    }
  }, [id]);

  const getProductDataById = async () => {
    try {
      let productByIdData = await getProductByID(Number(id));
      setProductData(productByIdData.data.data);
      setAmount(productByIdData.data.data.basicPrice);
      setPrice(productByIdData.data.data.basicPrice);

      let itemimg = productByIdData.data.data.itemImages;

      let index_img = 0;
      let tempImg = [];
      while (index_img < itemimg.length) {
        tempImg.push({
          ID: itemimg[index_img]?.id,
          thumbnail: `${NODE_API_URL}/static/itemImageFile/${itemimg[index_img]?.image}`,
          original: `${NODE_API_URL}/static/itemImageFile/${itemimg[index_img]?.image}`,
          check: false,
          Video: false,
        });
        index_img++;
      }

      setProductImages([...tempImg]);

      //set videos for preview
      let itemvideo = productByIdData.data.data.itemImages;

      let index_video = 0;
      let tempVideos = [];
      while (index_video < itemvideo.length) {
        tempVideos.push({
          ID: itemvideo[index_video]?.id,
          thumbnail: `${NODE_API_URL}/static/itemImageFile/${itemimg[index_img]?.image}`,
          original: `${NODE_API_URL}/static/itemImageFile/${itemimg[index_img]?.image}`,
          check: false,
          Video: true,
        });
        index_video++;
      }

      setProductVideo([...tempVideos]);
      //end
    } catch (error: any) {
      handleServerError(error);
    }
  };

  const handleMinusQty = async (currentQTY: number) => {
    // console.log(currentQTY, "currentQTY")
    try {
      if (cartQTY > 1) {
        let qty = Number(currentQTY - 1);
        setCartQTY(qty);
        setAmount(parseFloat(price) * qty);
      }
    } catch (error) {
      handleServerError(error);
    }
  };

  const handleAddQty = async (currentQTY: number) => {
    try {
      // if (MAXQTY > currentQTY) {
      let qty = Number(currentQTY + 1);
      setCartQTY(qty);
      setAmount(parseFloat(price) * qty);
      // } else {
      // toastAlert("warn", maxQTYMsg);
      // }
    } catch (error) {
      handleServerError(error);
    }
  };

  const AddToCart = async () => {
    try {
      /// onilne user adding kit in cart DB ////
      if (localStorage.getItem("user_data")) {
        let cartlist: any = await getCartList();
        cartlist = cartlist.data.data;

        //@ts-ignore
        let userId = JSON.parse(localStorage.getItem("user_data")).id;
        // return
        let Obj: any = {};
        setBtnLoader(true);

        Obj = {
          itemId: productData.id,
          quantity: cartQTY,
          amount: amount,
          userId: userId,
        };

        let productAdd: any = await addCart(Obj);        

        if (productAdd.data.data) {
          const isItemInCart = cartlist.some(
            (cartItem: any) => cartItem.itemId == productData.id
          );

          console.log(isItemInCart,"isItemInCart");
       
          if (!isItemInCart) {
            cartCountDispatch({ type: "ADD_ITEM" });
          } 
          // else {
          //   cartCountDispatch({ type: "ADD_ITEM" });
          // }

          toastAlert("success", productAdd.data.msg);
        }

        setBtnLoader(false);
      } else {
        /// Offline user adding item in local Storage cart////

        cart.add(
          {
            itemId: productData.id,
            amount: amount,
            realAmount: amount,
            itemImage: productData.itemImages[0].image,
            name: productData.itemName,
          },
          cartQTY
        );
        setShow(true);

        toastAlert("success", "successfully added to cart");
        setBtnLoader(false);
      }
    } catch (error: any) {
      setBtnLoader(false);
      handleServerError(error);
    }
  };

  return (
    <>
      <div className="container py-5">
        <Card>
          <CardContent>
            <div className="dark:bg-gray-800 py-8">
              <div className="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex flex-col md:flex-row -mx-4">
                  <div className="md:flex-1 px-4 flex-col">
                    <ImagePreview sourceArr={{ productImages, productVideo }} />
                    {/* {productData?.itemImages?.map((item: any) => {
                      return (
                        <div className="h-[75px] rounded-lg bg-gray-300 dark:bg-gray-700 mb-4">
                          <img
                            className="img-fluid w-full h-full object-cover"
                            src={
                              item?.image
                                ? `${NODE_API_URL}/static/itemImageFile/${item?.image}`
                                : "no_image.png"
                            }
                            alt="product"
                          />
                        </div>
                      );
                    })} */}
                  </div>
                  <div className="md:flex-1 px-4">
                    <div className="flex justify-between py-3">
                      <div>
                        <h2 className="text-2xl font-bold text-gray-800 dark:text-white mb-2">
                          {productData.itemName}
                        </h2>
                      </div>
                      <div>
                        <div>
                          <span className="text-2xl font-bold text-orange-400 dark:text-gray-300">
                            Price:
                          </span>
                          <span className="text-2xl font-bold text-orange-400 dark:text-gray-300">
                            {" "}
                            AED {amount}
                          </span>
                        </div>
                      </div>
                    </div>
                    {/* <p className="text-gray-600 dark:text-gray-300 text-sm mb-4">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Sed sed ante justo. Integer euismod libero id mauris
                      malesuada tincidunt.
                    </p> */}
                    <div className="flex mb-4 justify-between">
                      {/* <div className="mr-4">
                        <span className="font-bold text-gray-700 dark:text-gray-300">
                          Availability:
                        </span>
                        <span className="text-gray-600 dark:text-gray-300">
                          {" "}
                          In Stock
                        </span>
                      </div> */}
                      <div>
                        <span className="font-bold text-gray-700 dark:text-gray-300">
                          Category:
                        </span>
                        <span className="text-gray-600 dark:text-gray-300">
                          {" "}
                          {productData.Category?.title}
                        </span>
                      </div>
                    </div>
                    <div className="mb-5">
                      <span
                        className="textInner"
                        dangerouslySetInnerHTML={{
                          __html: productData.description,
                        }}
                      />
                    </div>
                    {/* <div>
                      <span className="font-bold text-gray-700 dark:text-gray-300">
                        Preparation:
                      </span>
                      <p className="text-gray-600 dark:text-gray-300 text-sm mt-2 mb-5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Sed sed ante justo. Integer euismod libero id mauris
                        malesuada tincidunt. Vivamus commodo nulla ut lorem
                        rhoncus aliquet. Duis dapibus augue vel ipsum pretium,
                        et venenatis sem blandit.
                      </p>
                    </div> */}
                    <div>
                      <div className="flex  mb-4">
                        <div className="w-1/4 px-2 flex flex-row justify-center items-center rounded-full bg-gray-200">
                          <Button variant="link" size="icon" className="h-4 ">
                            <Minus
                              className="h-4 w-4 text-gray-500"
                              onClick={() => handleMinusQty(cartQTY)}
                            />
                          </Button>
                          <div className="font-bold">
                            <h4 id="counting">{cartQTY}</h4>
                          </div>
                          <Button variant="link" size="icon" className="h-4">
                            <Plus
                              className="h-4 w-4 text-gray-500"
                              onClick={() => handleAddQty(cartQTY)}
                            />
                          </Button>
                        </div>
                        <div className="w-1/2 px-2">
                          <Button
                            className="w-full py-2 px-4 rounded-full font-bold "
                            variant="default"
                            onClick={() => AddToCart()}
                          >
                            Add to Cart
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
    </>
  );
}

export default ProductDetail;
