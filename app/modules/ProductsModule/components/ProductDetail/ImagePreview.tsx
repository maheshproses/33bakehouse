import Image from 'next/image'
import React from 'react'
import ImageVideoGallery from '@/components/ui/ImageVideoGallery/ImageVideoGallery'


function ImagePreview({sourceArr}:any) {

    return (
        <>
            <div>
                {/* <img className='img-fluid' src={`/cctv.png`} alt="cctv" /> */}
                <ImageVideoGallery
                    // sourceArr={{ productImages, productVideo }}
                    // diamond={diamond}

                    sourceArr={{ productImages: sourceArr?.productImages, productVideo: sourceArr?.productVideo }}
                />
            </div>
            {/* <Text size="h2" font="bold" className='mb-2'>ZANO CCTV Pro</Text>
            <Text size="h3">Zano CCTV Pro Camera 1080p</Text> */}
        </>
    )
}

export default ImagePreview