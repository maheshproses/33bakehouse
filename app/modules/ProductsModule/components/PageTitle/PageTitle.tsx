import { ChevronRightIcon, HomeIcon } from "@heroicons/react/24/solid";
import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import background from "../../../../../public/cake.jpg";
const PageTitle = (props:{pageTitle:any}) => {
  return (
    <>
      <div className="relative overflow-hidden ">
        <div className="justify-center flex-col items-center relative z-10">
          <div className="flex items-center justify-center py-10 flex-col pageTitle bg-slate-900 bg-opacity-50">
            <h1 className="text-gray-100 text-3xl">{props.pageTitle}</h1>
            <div className="flex items-center justify-center">
              <Link href="/">
                <HomeIcon width={15} className="py-2 fill-gray-100" />
              </Link>
              <div className="mr-2"></div>
              <ChevronRightIcon width={15} className="py-2 fill-gray-100" />
              <Link href="/Products" className="px-2 text-gray-100 text-sm">
                Product
              </Link>
              <ChevronRightIcon width={15} className="py-2 fill-gray-100" />
              <Link href="/Products" className="px-2 text-red-100 text-sm">
                Chair
              </Link>
            </div>
          </div>
        </div>
        <div className="absolute -top-60 bg-center z-0 ">
          {/* <img src={background} className="bg-center w-full " alt="product" /> */}
          <Image
            src={background}
            alt="product"
            // sizes="100vw"
            // style={{
            //   width: '100%',
            //   height: 'auto',
            // }}
          />
        </div>
      </div>
    </>
  );
};

export default PageTitle;
