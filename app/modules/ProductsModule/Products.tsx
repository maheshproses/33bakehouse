import type { NextPage } from "next";
import ProductList from "./components/ProductList/ProductList";
import Sidebar from "./components/Sidebar/Sidebar";
import PageTitle from "./components/PageTitle/PageTitle";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { getCategoryByID } from "@/app/api/apis";
import { handleServerError } from "@/app/utils/helpers";

const Products: NextPage = () => {
  const router = useRouter();
  const { categoryId } = router.query;

  const [productList, setProductList] = useState<any>([]);
  const [activeCategory, setActiveCategory] = useState<any>([]);


  useEffect(() => {
    if (categoryId) {
      getProductList();
    }
  }, [categoryId]);

  const getProductList = async () => {
    try {
      let categoryById = await getCategoryByID(Number(categoryId));
      setProductList(categoryById.data.data);

      let name = categoryById.data.data[0]?.Category.title;
      setActiveCategory(name);
    } catch (error: any) {
      handleServerError(error);
    }
  };

  return (
    <>
      <PageTitle pageTitle="Product" />
      <div className="container py-2">
        <div className="overflow-hidden rounded-[0.5rem] border bg-background shadow">
          {/* <div className="grid grid-rows-4 sm:grid-cols-1 md:grid-cols-5 max-w-md mx-auto w-full"> */}
          <div className="grid md:grid-cols-5 sm:grid-cols-1 sm:max-w-full grid-cols-1 w-full">
            <div className="">
              <Sidebar />
            </div>
            <div className="col-span-3 lg:col-span-4 lg:border-l">
              <ProductList
                //@ts-ignore
                productList={productList}
                activeCategory={activeCategory}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Products;
