import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import type { NextPage } from "next";

const ForgotPassword: NextPage = () => {
  return (
    <>
      <div className="items-center justify-center gap-6 rounded-md p-8 md:grid lg:grid-cols-2 xl:grid-cols-3 py-10 ">
        <div className="col-start-2 rounded-md border bg-background shadow">
          <div className="rounded-lg border bg-card text-card-foreground shadow-sm">
            <div className="flex flex-col p-6 space-y-1">
              <h3 className="font-semibold tracking-tight text-4xl text-center py-10">
                Forgot Password
              </h3>
              <div className="grid w-full max-w-sm items-center gap-1.5">
                <Label htmlFor="password">Password</Label>
                <Input type="password" id="password" placeholder="Password" />
              </div>
              <div className="grid w-full max-w-sm items-center gap-1.5">
                <Label htmlFor="Confirm password">Confirm password</Label>
                <Input type="Confirm password" id="Confirm password" placeholder="Password" />
              </div>
              <Button className="w-1/3 text-white bg-gray-800 hover:bg-gray-900 rounded-lg p-3 bottom-0 mx-1">
                        Reset Password
                    </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ForgotPassword;
