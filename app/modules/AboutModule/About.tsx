import type { NextPage } from "next";
import AboutModule from "../HomeModule/components/About/About";
import PageTitle from "../ProductsModule/components/PageTitle/PageTitle";
const About: NextPage = () => {
  return (
    <>
      <div className="max-w-full mx-auto">
        <PageTitle pageTitle="About" />
        <AboutModule />
      </div>
    </>
  );
};

export default About;
