import React from "react";

function WithIcons(Comp: any) {
  return function WrappedWithIcons(props: any) {
    let width = 16;
    let height = 16;
    let style = { fill: "black" };

    if (props?.size) {
      width = props?.size;
      height = props?.size;
    }

    if (props?.color) {
      style = { fill: props.color };
    }
    if (props?.style) {
      style = { ...style, ...props.style };
    }

    return <Comp width={width} height={height} style={style} {...props} />;
  };
}

export default WithIcons;
