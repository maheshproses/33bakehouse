"use client"

import AuthContext from "@/app/contexts/auth/auth.context";
import { useContext } from "react";

export const useCurrentUser = () => {
  const { authState, authDispatch }: any = useContext(AuthContext);
  return [authState.user_data];
};
