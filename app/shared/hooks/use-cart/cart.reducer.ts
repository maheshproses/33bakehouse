"use client"
const itemExist = (items: any, id: number) => {
  return items.findIndex((item: any) => item.itemId === id)
};

const addItemReducer = (state: any, action: any) => {
  
  let itemId = action.payload.itemId;
  let id = itemId;
  const existingItemIndex = itemExist(state.items, id);

  console.log(existingItemIndex,"this is existingItemIndex");
  if (existingItemIndex > -1) {
    const newState = [...state.items];
    newState[existingItemIndex].quantity += action.payload.quantity;
    newState[existingItemIndex].amount = newState[existingItemIndex].realAmount * newState[existingItemIndex].quantity;
    return newState;
  }
  return [...state.items, action.payload];
};

const removeItemReducer = (state: any, action: any) => {
  const existingItemIndex = action.payload.id;
  let temparr = state.items
  if (existingItemIndex >= 0) {
    temparr.splice(existingItemIndex, 1);
  }
  return [...temparr];
};

const updateQuantity = (state: any, action: any) => {
  
  return state.items.map((item: any, index: number) => {
    if (index === action.payload.id) {
      
      const newItem = { ...item };

      if (action.payload.action === "increment") {        
        newItem.quantity = item.quantity + 1;
        newItem.amount = item.realAmount * newItem.quantity
      }

      if (action.payload.action === "decrement" && item.quantity > 1) {
        newItem.quantity = item.quantity - 1;
        newItem.amount = item.realAmount * newItem.quantity
      }
      return newItem;
    }
    return item;
  });
};

export const reducer = (state: any, action: { type: string; payload?: any }) => {

  switch (action.type) {
    case "ADD_ITEM":
      return { items: addItemReducer(state, action) };
    case "REMOVE_ITEM":
      return { items: removeItemReducer(state, action) };
    case "CLEAR_CART":
      return { items: [] };
    case "UPDATE_QUANTITY":
      return { items: updateQuantity(state, action) };
    case "SYNC_ITEMS":
      return { items: [...action.payload] };
  }
};
