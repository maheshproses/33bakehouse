"use client"
import React, {
  useReducer,
  useContext,
  createContext,
  useMemo,
  useEffect,
} from "react";

import { LS_KEYS } from "@/app/utils/constants";
import { CartContextType } from "./cart.types";
import { reducer } from "./cart.reducer";
import useLocalStorage from "../use-local-storage";
import { toastAlert } from "../../components/Layout";

const CartContext = createContext<CartContextType>(null);


export const CartProvider = ({ children, initialCart = [] }) => {

  const [state, dispatch] = useReducer(reducer, { items: initialCart });

  const [cart, setCart] = useLocalStorage(LS_KEYS.userCart, state.items);  
  

  const syncItems = (items: any[]) => {
    dispatch({ type: "SYNC_ITEMS", payload: items });
  };

  useEffect(() => {
    syncItems(cart);
  }, []);

  useEffect(() => {     
    console.log("check this");      
    setCart(state.items);
  }, [state.items]);

  const count = useMemo(() => {
    return state.items.length;
  }, [state.items]);

  const addItemHandler = (item: any, quantity = 1) => {    
    dispatch({ type: "ADD_ITEM", payload: { ...item, quantity } });
  };

  const removeItemHandler = (id: any) => {
    dispatch({ type: "REMOVE_ITEM", payload: { id } });
    toastAlert("success","Items Deleted successfully")
  };

  const clearCartHandler = () => {
    dispatch({ type: "CLEAR_CART" });
  };

  const isInCartHandler = (id: any) => {
    return state.items.some((item: any) => item.id === id);
  };

  const updateQuantity = (id:number, action: 'increment'|'decrement') => {
    dispatch({type: 'UPDATE_QUANTITY', payload: {id, action}})
  }

  return (
    <CartContext.Provider
      value={{
        items: state.items,
        add: addItemHandler,
        remove: removeItemHandler,
        clear: clearCartHandler,
        inCart: isInCartHandler,
        calculateTotal: () => 2,
        count,
        syncItems,
        updateQuantity
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
