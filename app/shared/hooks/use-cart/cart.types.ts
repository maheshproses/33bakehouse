"use client"
export type CartContextType = {
    items: any[],
    count: number,
    add: (item: any, quantity?: number) => void,
    remove: (id: number|string, quantity?: number) => void
    clear: () => void
    inCart: (id: number|string) => boolean
    calculateTotal: () => number
    updateQuantity: (id:number, action: 'increment'|'decrement') => void
    syncItems: (items:any[]) => void
}