"use client";

import Footer from "./Footer/Footer";
import Header from "./Header/Header";
import { Roboto_Mono } from "next/font/google";
import { Raleway, Nosifer } from "next/font/google";
import { Roboto } from "next/font/google";
import "@/styles/globals.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { NextAuthProvider } from "@/app/provider";
import { CartProvider } from "../hooks/use-cart/use-cart.hook";
import { useEffect, useState } from "react";

export const toastAlert = (type: string, msg: string) => {
  if (type == "success") toast.success(msg);
  else if (type == "warn") toast.warn(msg);
  else if (type == "error") toast.error(msg);
  else toast.info("An Alert Problem");
};

// const robotoMono = Roboto_Mono({
//   subsets: ['latin'],
//   display: 'swap',
//   variable: '--font-roboto-mono',
// })
const roboto = Roboto({
  weight: ["900"],
  subsets: ["latin"],
});

const Layout = ({ children }: any) => {
  const [hasmounted, sethasmounted] = useState(false);

  useEffect(() => {
    sethasmounted(true);
  }, []);
  if (!hasmounted) {
    return null;
  }
  return (
    <>
      <NextAuthProvider>
      <Header />
      <div>
        {/* <Sidebar /> */}
        <div id="content">{children}</div>
        {/* use for alert */}
        <ToastContainer
          position="top-center"
          autoClose={1500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
      <Footer />
      </NextAuthProvider>
    </>
  );
};

export default Layout;
