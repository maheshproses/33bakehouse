"use client";
import { getAllCategory } from "@/app/api/apis";
import AuthContext from "@/app/contexts/auth/auth.context";
import CartCountContext from "@/app/contexts/cartCount/cartCount.context";
import {
  Menubar,
  MenubarContent,
  MenubarItem,
  MenubarMenu,
  MenubarShortcut,
  MenubarTrigger,
} from "@/components/ui/menubar";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { Lock, LogOut, Menu, ShoppingCart, UserCircle2 } from "lucide-react";
import type { NextPage } from "next";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

const SHEET_SIDES = ["top", "right", "bottom", "left"] as const;

type SheetSide = (typeof SHEET_SIDES)[number];

// type actionTypes = {
//   name: "My Profile" | "My Orders" | "Change Password" | "Logout";
//   id: string;
//   icon?: any;
// };
// export const options: actionTypes[] = [
//   { name: "My Profile", id: "Profile", icon: <UserIcon /> },
//   { name: "My Orders", id: "MyOrders", icon: <CartIcon /> },
//   { name: "Change Password", id: "ChangePass", icon: <Lock /> },
//   { name: "Logout", id: "Login", icon: <LogoutIcon /> },
// ];

const NavigationMenu: NextPage = () => {
  const { data: session } = useSession();
  const user = session?.user;

  // const [user] = useCurrentUser();
  const { authState }: any = useContext(AuthContext);

  const router = useRouter();
  const { cartCountDispatch }: any = useContext(CartCountContext);
  const { authDispatch }: any = useContext(AuthContext);
  const [categoryList, setCategoryList] = useState<any>([]);
  const [hasmounted, sethasmounted] = useState(false);

  const handleChangeLogout = () => {
    localStorage.clear();
    // cartCountDispatch({ type: "EMPTY_ITEMS" });
    authDispatch({ type: "SIGN_OUT" });
    router.push(`/Login`);
  };

  useEffect(() => {
    sethasmounted(true);
    getCategoryList();
  }, []);

  const getCategoryList = async () => {
    try {
      let categoryList = await getAllCategory();
      setCategoryList(categoryList.data.data);
    } catch (error: any) {}
  };

  if (!hasmounted) {
    return null;
  }
  return (
    <>
      <div className="max-sm:hidden max-md:hidden flex justify-center items-center">
        <Menubar>
          <MenubarMenu>
            <MenubarTrigger>
              <Link href="/">Home</Link>
            </MenubarTrigger>
          </MenubarMenu>
          <MenubarMenu>
            <MenubarTrigger>
              <Link href="/About">About</Link>
            </MenubarTrigger>
          </MenubarMenu>
          <MenubarMenu>
            <MenubarTrigger>Categories</MenubarTrigger>
            <MenubarContent>
              {categoryList.map((item: any) => {
                return (
                  <MenubarItem key={item?.id}>
                    <Link
                      href={`/Profile`}
                      className="flex flex-row justify-between w-full items-center"
                    >
                      <div className="flex justify-between w-full">
                        <Link
                          href={`/Products/?categoryId=${item.id}`}
                          className="link"
                        >
                          <div>{item.title}</div>
                        </Link>
                      </div>
                    </Link>
                  </MenubarItem>
                );
              })}
            </MenubarContent>
          </MenubarMenu>
          {authState.authenticated ? null : (
            <MenubarMenu>
              <MenubarTrigger>
                <Link href="/TrackOrder">Track Order</Link>
              </MenubarTrigger>
            </MenubarMenu>
          )}
          <MenubarMenu>
            <MenubarTrigger>
              {authState.authenticated ? null : (
                <>
                  <Link href={`/Registration`}>Sign Up</Link>
                  &nbsp;/&nbsp;
                  <Link href={`/Login`}>Login</Link>
                </>
              )}
            </MenubarTrigger>
          </MenubarMenu>
          {authState.authenticated ? (
            <MenubarMenu>
              <MenubarTrigger>
                {/* <Link href="#"> */}
                <UserCircle2 />
                {/* </Link> */}
              </MenubarTrigger>
              <MenubarContent>
                <MenubarItem>
                  <Link
                    href={`/Profile`}
                    className="flex flex-row justify-between w-full items-center"
                  >
                    <div className="flex justify-between w-full">
                      <div> My Profile</div>
                      <div className="flex justify-center items-center">
                        <MenubarShortcut>
                          <UserCircle2 size={15} />
                        </MenubarShortcut>
                      </div>
                    </div>
                  </Link>
                </MenubarItem>
                <MenubarItem>
                  <Link
                    href={`/MyOrder`}
                    className="flex flex-row justify-between w-full items-center"
                  >
                    My Order{" "}
                    <MenubarShortcut>
                      <ShoppingCart size={15} />
                    </MenubarShortcut>
                  </Link>
                </MenubarItem>
                <MenubarItem>
                  <Link
                    href={`/ChangePassword`}
                    className="flex flex-row justify-between w-full items-center"
                  >
                    Change Password{" "}
                    <MenubarShortcut>
                      <Lock size={15} />
                    </MenubarShortcut>
                  </Link>
                </MenubarItem>
                <MenubarItem>
                  <div
                    className="flex flex-row justify-between w-full items-center cursor-pointer"
                    onClick={handleChangeLogout}
                  >
                    {/* <Link
                  href={""}
                  className="flex flex-row justify-between w-full items-center"
                > */}
                    Logout{" "}
                    <MenubarShortcut>
                      <LogOut size={15} />
                    </MenubarShortcut>
                    {/* </Link> */}
                  </div>
                </MenubarItem>
              </MenubarContent>
            </MenubarMenu>
          ) : null}
        </Menubar>
      </div>
      <div className="sm:hidden">
        <Sheet>
          <SheetTrigger>
            <Menu />
          </SheetTrigger>
          <SheetContent side="left">
            <SheetHeader>
              <SheetTitle>33BAKEHOUSE</SheetTitle>
              <SheetDescription>
                <ul className="text-left font-medium flex flex-col p-2 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                  <li>
                    <Link
                      href="/"
                      className="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500"
                      aria-current="page"
                    >
                      Home
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/About"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      About
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/Products"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      Categories
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/Profile"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      My Profile
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="#"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      My Order
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/ChangePassword"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      Change Password
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="#"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      Logout
                    </Link>
                  </li>
                  {/* <li>
                    <Link
                      href="#"
                      className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
                    >
                      Contact
                    </Link>
                  </li> */}
                </ul>
              </SheetDescription>
            </SheetHeader>
          </SheetContent>
        </Sheet>
      </div>

      {/* <div className="flex items-center justify-end">
      <div className="group relative cursor-pointer py-2">
        <div className="flex items-center justify-between space-x-5 bg-white px-4">
          <Link className="menu-hover my-2 py-2 text-base font-medium text-black lg:mx-4">
            Our Products
          </Link>
          <span>
            <ChevronDown/>
          </span>
        </div>
        <div
          className="invisible absolute z-50 flex w-full flex-col bg-gray-100 py-1 px-4 text-gray-800 shadow-xl group-hover:visible">
          <Link className="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product One</Link>
          <Link className="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product Two</Link>
          <Link className="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product Three</Link>
          <Link className="my-2 block border-b border-gray-100 py-1 font-semibold text-gray-500 hover:text-black md:mx-2">Product Four</Link>
        </div>
      </div>
    </div> */}
    </>
  );
};

export default NavigationMenu;
