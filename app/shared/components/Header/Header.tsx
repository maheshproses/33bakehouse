"use client";
import AuthContext from "@/app/contexts/auth/auth.context";
import CartCountContext from "@/app/contexts/cartCount/cartCount.context";
import { ShoppingBag } from "lucide-react";
import Link from "next/link";
import { useContext } from "react";
import { useCart } from "../../hooks/use-cart/use-cart.hook";
import NavigationMenu from "../NavigationMenu/NavigationMenu";
import { publicPathName } from "@/app/utils/constants";

const Header = () => {
  // const [user] = useCurrentUser();
  const { authState }: any = useContext(AuthContext);
  const cart = useCart();
  const { cartCountState }: any = useContext(CartCountContext);  

  return (
    <>
      <div className="max-w-[100%] sticky top-0 shadow z-20 bg-white">
        <div className="container top-0 z-10 bg-white">
          <div className="flex flex-row xl:flex-row-reverse md:flex-row-reverse justify-between menuBar py-2 lg:m-4 sm:m-5 md:m-3 xl:m-6">
            <div className="flex">
              <NavigationMenu />
              <div className="ms-1 border p-2 border-gray-200 rounded-md max-sm:hidden max-md:hidden">
                <Link
                  href="/CartItemList"
                  className="relative items-center text-sm font-medium text-center text-black "
                >
                  <ShoppingBag width={20} />
                  {!authState.authenticated ? (
                    <div className="absolute inline-flex items-center justify-center w-5 h-5 text-[10px] font-bold text-white bg-red-500 border-white rounded-full -top-3 -right-2 dark:border-gray-900">
                      {/* 0 */}
                      <span>{cart.count}</span>
                    </div>
                  ) : (
                    <div className="absolute inline-flex items-center justify-center w-5 h-5 text-[10px] font-bold text-white bg-red-500 border-white rounded-full -top-3 -right-2 dark:border-gray-900">
                      {/* 0 */}
                      {/* <span> */}
                      {cartCountState?.count}
                      {/* </span> */}
                    </div>
                  )}
                </Link>
              </div>
            </div>
            <div>
              <div className="flex justify-between">
                <Link href="/">
                  <img
                    src={`${publicPathName}/33BH_01.png`}
                    className="max-w-screen-sm"
                    width={125}
                    alt="33backhouse"
                  />
                </Link>
              </div>
            </div>
            <div className="ms-1 border p-2 border-gray-200 rounded-md md:hidden lg:bg-green-600">
              <Link
                href="/CartItemList"
                className="relative items-center text-sm font-medium text-center text-black "
              >
                <ShoppingBag width={20} />
                <div className="absolute inline-flex items-center justify-center w-5 h-5 text-[10px] font-bold text-white bg-red-500 border-white rounded-full -top-3 -right-2 dark:border-gray-900">
                  {/* 20 */}
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
