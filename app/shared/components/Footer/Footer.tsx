"use client";
import React from "react";
import Link from "next/link";
import { Facebook, Instagram, Linkedin, Twitter } from "lucide-react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

const Footer = () => {
  return (
    <footer className="bg-gray-900">
      <div
        className="
      container
      flex flex-col flex-wrap
      py-16
      mx-auto
      md:items-center
      lg:items-start
      md:flex-row md:flex-nowrap
    "
      >
        <div className="flex-shrink-0 md:w-80 mx-auto text-center md:mx-0 md:text-left">
          <Link href={"/"} className="text-2xl text-gray-600">
            33BAKEHOUSE
          </Link>
          <p className="mt-2 text-xs text-justify text-gray-400">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi
            provident pariatur suscipit nihil. Animi dicta explicabo officia
            quae eos pariatur deleniti a, ratione asperiores porro nobis iste
            ducimus! Assumenda, ut?.
          </p>
          <div className="flex mt-4">
            <Input
              type="email"
              placeholder="Email"
              className="rounded-r-none"
            />
            <Button variant="destructive" className="rounded-l-none">
              Subscribe
            </Button>
          </div>
          <div className="flex mt-4 space-x-4 lg:mt-2">
            <Link href={""}>
              <Facebook className="text-blue-500" />
            </Link>
            <Link href={""}>
              <Twitter className="text-sky-300" />
            </Link>
            <Link href={""}>
              <Instagram className="text-pink-500" />
            </Link>
            <Link href={""}>
              <Linkedin className="text-blue-400" />
            </Link>
          </div>
        </div>
        <div className="justify-between w-full lg:flex lg:mt-0 mt-5">
          <div className="w-full px-4 lg:w-1/3 md:w-1/2">
            <h2 className="mb-2 tracking-widest text-gray-400">Product Tags</h2>
            <ul className="text-sm list-none flex justify-start">
              <li className="rounded bg-gray-500 inline-block mr-1">
                <Link href={"/"} className="text-gray-300 text-xs p-2">
                  Product 1
                </Link>
              </li>
              <li className="rounded bg-gray-500 inline-block mr-1">
                <Link href={"/"} className="text-gray-300 text-xs p-2">
                  Product 2
                </Link>
              </li>
              <li className="rounded bg-gray-500 inline-block mr-1">
                <Link href={"/"} className="text-gray-300 text-xs p-2">
                  Product 3
                </Link>
              </li>
              <li className="rounded bg-gray-500 inline-block mr-1">
                <Link href={"/"} className="text-gray-300 text-xs p-2">
                  Product 4
                </Link>
              </li>
            </ul>
          </div>
          <div className="w-full px-4 lg:w-1/3 md:w-1/2">
            <h2 className="mb-2  tracking-widest text-gray-400">Quick Links</h2>
            <ul className="mb-8 space-y-2 text-sm list-none">
              <li>
                <Link href={"/"} className="text-gray-300 mr-2">
                  Link 1
                </Link>
              </li>
              <li>
                <Link href={"/"} className="text-gray-300 mr-2">
                  Link 2
                </Link>
              </li>
              <li>
                <Link href={"/"} className="text-gray-300 mr-2">
                  Link 3
                </Link>
              </li>
              <li>
                <Link href={"/"} className="text-gray-300">
                  Link 4
                </Link>
              </li>
            </ul>
          </div>
          <div className="w-full px-4 lg:w-1/3 md:w-1/2">
            <h2 className="mb-2 tracking-widest text-gray-400">Information</h2>
            <div className="text-gray-300 text-sm">
              Connaugt Road Central Suite 18B, 148 New Yankee +1 (555) 333 22 11
              info@33bakehouse.com
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-center border-t-2 border-gray-600 py-5 items-center">
        <p className="text-center text-gray-500 text-sm">
          @2023 All rights reserved by 33BAKEHOUSE.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
