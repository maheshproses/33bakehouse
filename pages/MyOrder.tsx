import Layout from "@/app/shared/components/Layout";
import type { NextPage } from "next";
import MyOrderModule from '@/app/modules/MyOrders/MyOrders'
const MyOrder: NextPage = () => {
  return (
    <>
    <Layout>
        <MyOrderModule/>
    </Layout>
    </>
  )
}

export default MyOrder