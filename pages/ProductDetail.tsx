import Layout from "@/app/shared/components/Layout";
import type { NextPage } from "next";
import ProductDetailModule  from '@/app/modules/ProductsModule/components/ProductDetail/ProductDetail'
const ProductDetail: NextPage = () => {
  return (
    <>
    <Layout>
        <ProductDetailModule/>
    </Layout>
    </>
  )
}

export default ProductDetail