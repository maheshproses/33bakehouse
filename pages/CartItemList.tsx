import Layout from "@/app/shared/components/Layout";
import CartItemListModule from "@/app/modules/CartModule/components/CartItemList";
import React from "react";

function CartItemList() {
  return (
    <Layout>
      <CartItemListModule />
    </Layout>
  );
}

export default CartItemList;
