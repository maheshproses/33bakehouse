import Layout from "@/app/shared/components/Layout";
import type { NextPage } from "next";
import CartModule from '@/app/modules/CartModule/Cart'
const Cart: NextPage = () => {
  return (
    <>
    <Layout>
        <CartModule/>
    </Layout>
    </>
  )
}

export default Cart