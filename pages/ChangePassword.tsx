import Layout from '@/app/shared/components/Layout'
import React from 'react'
import ChangePasswordModule from "@/app/modules/ChangePasswordModule/ChangePassword";


function ChangePassword() {
  return (
    <Layout>

    <ChangePasswordModule />
    </Layout>
  )
}

export default ChangePassword