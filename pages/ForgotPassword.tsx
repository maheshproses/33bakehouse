import type { NextPage } from "next";
import ForgotPasswordModule from "@/app/modules/ForgotPasswordModule/ForgotPassword";
import Layout from "@/app/shared/components/Layout";
const ForgotPassword: NextPage = () => {
  return (
    <>
    <Layout>
    <ForgotPasswordModule/>
    </Layout>
    </>
  )
}

export default ForgotPassword