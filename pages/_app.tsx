import type { AppProps } from "next/app";
import "../styles/globals.css";
import "nprogress/nprogress.css";
import NProgress from "nprogress";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { UiProvider } from "@/app/contexts/ui/ui.provider";
import { AuthProvider } from "@/app/contexts/auth/auth.provider";
import { CartProvider } from "@/app/shared/hooks/use-cart/use-cart.hook";
import { NextAuthProvider } from "@/app/provider";
import { CartCountProvider } from "@/app/contexts/cartCount/cartCount.provider";

NProgress.configure({ showSpinner: false, minimum: 0.3 });

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();

  useEffect(() => {
    router.events.on("routeChangeStart", () => {
      NProgress.start();
    });

    router.events.on("routeChangeError", () => {
      NProgress.done();
    });

    router.events.on("routeChangeComplete", () => {
      NProgress.done();
    });

    return () => {
      router.events.off("routeChangeStart", () => {});

      router.events.off("routeChangeError", () => {});

      router.events.off("routeChangeComplete", () => {});
    };
  }, []);

  return (
    <NextAuthProvider>
      <UiProvider>
        <CartCountProvider>
          <CartProvider>
            <AuthProvider>
              <Component {...pageProps} />
            </AuthProvider>
          </CartProvider>
        </CartCountProvider>
      </UiProvider>
    </NextAuthProvider>
  );
}

export default MyApp;
