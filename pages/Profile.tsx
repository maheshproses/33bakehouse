import Layout from "@/app/shared/components/Layout";
import type { NextPage } from "next";
import ProfileModule from '@/app/modules/ProfileModule/Profile'
const Profile: NextPage = () => {
  return (
    <>
    <Layout>
        <ProfileModule/>
    </Layout>
    </>
  )
}

export default Profile