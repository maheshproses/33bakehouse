import type { NextPage } from "next";
import ProductsModule from '@/app/modules/ProductsModule/Products'
import Layout from "@/app/shared/components/Layout";
const Products: NextPage = () => {
  return (
    <>
     <Layout>
    <ProductsModule/>
    </Layout>
    </>
  )
}

export default Products