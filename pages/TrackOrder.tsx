import Layout from '@/app/shared/components/Layout'
import React from 'react'
import TrackOrderModule from '@/app/modules/MyOrders/TrackOrder'

function TrackOrder() {
  return (
    <>
    <Layout>
        <TrackOrderModule />
    </Layout>
    </>
  )
}

export default TrackOrder