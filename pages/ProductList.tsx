import type { NextPage } from "next";
import ProductListModule from '@/app/modules/ProductsModule/components/ProductList/ProductList'
import Layout from "@/app/shared/components/Layout";
const ProductList: NextPage = () => {
  return (
    <>
     <Layout>
     <ProductListModule/>
    </Layout>
    </>
  )
}

export default ProductList