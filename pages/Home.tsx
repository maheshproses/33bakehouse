import Layout from "@/app/shared/components/Layout";
import type { NextPage } from "next";
import HomeModule from '../app/modules/HomeModule/Home'
const Home: NextPage = () => {
  return (
    <>
    <Layout>
        <HomeModule/>
    </Layout>
    </>
  )
}

export default Home